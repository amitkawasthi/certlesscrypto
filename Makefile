# Makefile

CXX       ?= g++
CXXFLAGS = -O2 -Wall $(IFLAGS) 
LDFLAGS = -L/usr/local/lib
IFLAGS	= -I$(CURDIR)/src -I/usr/local/include/Sockets -I/usr/local/include/pbc -I/usr/local/include
LFLAGS	=  -lSockets -lgmp -lpbc -lcrypto -lssl -lpthread

INC_FILES := $(wildcard src/*/*.h) \
             $(wildcard src/*/*/*.h)

SRC_COMMON_FILES := $(wildcard src/common/*.cpp) \
             $(wildcard src/common/*/*.cpp)

# pps files
SRC_PPS_FILES := $(wildcard src/pps/*.cpp) \
             $(wildcard src/pps/*/*.cpp)

OBJ_PPS_FILES := $(addsuffix .o,$(basename $(SRC_PPS_FILES))) \
			$(addsuffix .o,$(basename $(SRC_COMMON_FILES)))

# kgc files
SRC_KGC_FILES := $(wildcard src/kgc/*.cpp) \
             $(wildcard src/kgc/*/*.cpp)

OBJ_KGC_FILES := $(addsuffix .o,$(basename $(SRC_KGC_FILES))) \
			$(addsuffix .o,$(basename $(SRC_COMMON_FILES)))

# bob files
SRC_BOB_FILES := $(wildcard src/bob/*.cpp) \
             $(wildcard src/bob/*/*.cpp)

OBJ_BOB_FILES := $(addsuffix .o,$(basename $(SRC_BOB_FILES))) \
			$(addsuffix .o,$(basename $(SRC_COMMON_FILES)))

# alice files
SRC_ALICE_FILES := $(wildcard src/alice/*.cpp) \
             $(wildcard src/alice/*/*.cpp)

OBJ_ALICE_FILES := $(addsuffix .o,$(basename $(SRC_ALICE_FILES))) \
			$(addsuffix .o,$(basename $(SRC_COMMON_FILES)))

# test files
SRC_TEST_FILES := $(wildcard src/test/*.cpp) \
             $(wildcard src/test/*/*.cpp)

OBJ_TEST_FILES := $(addsuffix .o,$(basename $(SRC_TEST_FILES))) \
			$(addsuffix .o,$(basename $(SRC_COMMON_FILES)))
			
all: pps kgc bob alice

clean: FORCE
	exec rm -f -- pps kgc bob alice test $(OBJ_PPS_FILES) $(OBJ_KGC_FILES) $(OBJ_BOB_FILES) $(OBJ_ALICE_FILES) $(OBJ_TEST_FILES)

pps: $(OBJ_PPS_FILES)
	exec $(CXX) $(LDFLAGS) -o pps $(OBJ_PPS_FILES) $(LFLAGS)

kgc: $(OBJ_KGC_FILES)
	exec $(CXX) $(LDFLAGS) -o kgc $(OBJ_KGC_FILES) $(LFLAGS)

bob: $(OBJ_BOB_FILES)
	exec $(CXX) $(LDFLAGS) -o bob $(OBJ_BOB_FILES) $(LFLAGS)

alice: $(OBJ_ALICE_FILES)
	exec $(CXX) $(LDFLAGS) -o alice $(OBJ_ALICE_FILES) $(LFLAGS)

test: $(OBJ_TEST_FILES)
	exec $(CXX) $(LDFLAGS) -o test $(OBJ_TEST_FILES) $(LFLAGS)
	
%.o: %.cpp $(INC_FILES)
	exec $(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

.PHONY: FORCE
