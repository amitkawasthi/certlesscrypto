/*
 * Title:
 *     Bob
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Bob - receiver
 *
 * NOTE:
 *     <note>
 */

#include <ListenSocket.h>
#include "common/utils.h"
#include "common/sockets/ppsclientsocket.h"
#include "bob/bobclientsocket.h"
#include "bob/bobconfig.h"
#include "bob/bobhandler.h"
#include "bob/bobserversocket.h"

/// Sets up Bob's key-pair and public parameters
void setup(Arguments & arguments)
{
    BobConfig bobConfig;
    PubParam  pp;
    Key       k(pp.pairing);
    PubKey    pk(pp.pairing);
    std::stringstream ss;

    ss << arguments["setup"];

    Utils::setConfig(arguments, "addr", bobConfig.cc.addr);
    Utils::setConfig(arguments, "port", bobConfig.cc.port);
    Utils::setConfig(arguments, "login", bobConfig.kgcLogin);
    Utils::setConfig(arguments, "pass", bobConfig.kgcPass);
    bobConfig.setPubParam(pp);
    bobConfig.setKey(k);
    bobConfig.setPubKey(pk);

    std::cout << "Downloading Public Parameters... ";

    try
    {
        PacketHandler h(bobConfig.getPubParam());
        PpsClientSocket ppsSock(h);

        ppsSock.Open(bobConfig.cc.addr, bobConfig.cc.port);
        h.Add(&ppsSock);
        while (h.GetCount())
        {
            h.Select();
        }

        std::cout << bobConfig.getPubParam() << std::endl;
        std::cout << "Setting up key-pair... ";

        BobClientSocket bobSock(h, bobConfig);

        bobSock.Open(bobConfig.getPubParam().kgcAddr,
            bobConfig.getPubParam().kgcPort);
        h.Add(&bobSock);
        while (h.GetCount())
        {
            h.Select();
        }

        bobConfig.save();
    }
    catch (ClException e)
    {
        e.print();
    }
}

/// Runs Bob's server
void run(Arguments & arguments, BobConfig & bobConfig)
{

    BobHandler h(bobConfig);
    ListenSocket<BobServerSocket> l(h);

    if (l.Bind(bobConfig.port))
    {
        std::cerr << "Bind() failed" << std::endl;
        exit(-1);
    }
    Utility::ResolveLocal();
    h.Add(&l);
    while (true)
    {
        h.Select();
    }
}

int main(int argc, char * argv[])
{
    Arguments arguments = Utils::parseParams(argc, argv);
    BobConfig bobConfig;

    if (arguments.end() != arguments.find("setup"))
    {
        setup(arguments);
    }
    else
    {
        try
        {
            bobConfig.load();
            std::cout << bobConfig;
            run(arguments, bobConfig);
        }
        catch (ClException e)
        {
            e.print();
        }
    }
    return 0;
}
