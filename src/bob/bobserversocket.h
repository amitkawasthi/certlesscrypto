/*
 * Title:
 *     Bob Server Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Bob server socket
 *
 * NOTE:
 *     <note>
 */

#ifndef _BOBSOCKET_H_
#define _BOBSOCKET_H_

#include "common/utils.h"
#include "common/packets/packet.h"
#include "common/sockets/packetsocket.h"
#include "bob/bobhandler.h"

class BobServerSocket
    : public PacketSocket
{
public:

    BobServerSocket(ISocketHandler & h);

    BobHandler & getBobHandler() const;

    void onPubKeyReqPacket(const PubKeyReqPacket & packet);

    void onMsgPacket(const MsgPacket & packet);
};

#endif  // _BOBSOCKET_H_
