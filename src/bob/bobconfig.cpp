/*
 * Title:
 *     Bob Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Configuration of Bob
 */

#include "bob/bobconfig.h"

BobConfig::BobConfig()
    : cc(clientConf)
    , port(6002)
    , kgcLogin("bob@domain.org")
    , kgcPass("pass")
    , m_pp(NULL)
    , m_qId(NULL)
    , m_pk(NULL)
{
    cc.addr = "localhost";
    cc.port = 6001;
}

BobConfig::~BobConfig()
{
    delete m_pk;
    delete m_qId;
    delete m_pp;
}

void BobConfig::load() throw(ClException)
{
    try
    {
        cc.load();
        if (NULL == m_pp)
        {
            m_pp = new PubParam();
        }
        m_pp -> load(bobPpConf);
        if (NULL == m_qId)
        {
            m_qId = new Key(m_pp -> pairing);
        }
        m_qId -> load(bobQidConf, m_pp -> pairing);
        if (NULL == m_pk)
        {
            m_pk = new PubKey(m_pp -> pairing);
        }
        m_pk -> load(bobPkConf, m_pp -> pairing);

        std::ifstream input(bobConf.c_str());

        if (input.good())
        {
            input >> kgcLogin >> kgcPass >> port;
            input.close();
        }
        else
        {
            throw ClException("BobConfig::load: cannot open file");
        }
    }
    catch (ClException e)
    {
        throw ClException("BobConfig::load:\n" + e.toString());
    }
}

void BobConfig::save() throw(ClException)
{
    if (NULL == m_pp)
    {
        throw ClException("BobConfig::save: Empty public parameters");
    }
    else if (NULL == m_qId)
    {
        throw ClException("BobConfig::save: Empty private key");
    }
    if (NULL == m_pk)
    {
        throw ClException("BobConfig::save: Empty public key");
    }
    else
    {
        std::ofstream output(bobConf.c_str());

        output << kgcLogin << std::endl << kgcPass << std::endl
            << port << std::endl;
        output.close();

        cc.save();
        m_pp  -> save(bobPpConf);
        m_qId -> save(bobQidConf);
        m_pk  -> save(bobPkConf);
    }
}

PubParam & BobConfig::getPubParam() const throw(ClException)
{
    if (NULL == m_pp)
    {
        throw ClException("BobConfig::getPubParam: Empty public parameters");
    }
    else
    {
        return (*m_pp);
    }
}

void BobConfig::setPubParam(const PubParam & pp)
{
    delete m_pp;
    m_pp = new PubParam(pp);
}

Key & BobConfig::getKey() const throw(ClException)
{
    if (NULL == m_qId)
    {
        throw ClException("BobConfig::getKey: Empty private key");
    }
    else
    {
        return (*m_qId);
    }
}

void BobConfig::setKey(const Key & k)
{
    delete m_qId;
    m_qId = new Key(k);
}

PubKey & BobConfig::getPubKey() const throw(ClException)
{
    if (NULL == m_pk)
    {
        throw ClException("BobConfig::getPubKey: Empty public key");
    }
    else
    {
        return (*m_pk);
    }
}

void BobConfig::setPubKey(const PubKey & pk)
{
    delete m_pk;
    m_pk = new PubKey(pk);
}

std::ostream & operator<<(std::ostream & os, const BobConfig & c)
{
    try
    {
        os << c.getPubParam();
    }
    catch (ClException e)
    {
        os << "KgcConfig::operator<<: Empty System Parameters" << std::endl;
        e.print();
    }

    os << c.cc << std::endl
       << "Bob configuration" << std::endl
       << "KGC login" << std::endl
       << Utils::ident(c.kgcLogin)
       << "KGC password" << std::endl
       << Utils::ident(c.kgcPass)
       << "Bob's port" << std::endl
       << Utils::ident(Utils::toString(c.port));

    return os;
}
