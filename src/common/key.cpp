/*
 * Title:
 *     Public Parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Key used by CL-PKE/IBE cryptosystem (as public, private, private partial
 *     keys)
 */

#include "common/key.h"

Key::Key(const pairing_t & pairing)
{
    element_init_G2(key, const_cast<pairing_t &>(pairing));
}

Key::Key(const std::string & str, const pairing_t & pairing) throw(ClException)
{
    element_init_G2(key, const_cast<pairing_t &>(pairing));
    try
    {
        Utils::strToPoint(key, str);
    }
    catch(ClException e)
    {
        element_clear(key);
        throw ClException("Key::Key:\n" + e.toString());
    }
}

Key::Key(const element_t & dId, const element_t & x)
{
    element_init_same_as(key, const_cast<element_t &>(dId));
    element_pow_zn(key, const_cast<element_t &>(dId),
        const_cast<element_t &>(x));
}

Key::Key(const Key & pk)
{
    element_init_same_as(key, const_cast<Key &>(pk).key);
    element_set(key, const_cast<Key &>(pk).key);
}

Key::~Key()
{
    element_clear(key);
}

Key & Key::operator=(const Key & pk)
{
    element_clear(key);
    element_init_same_as(key, const_cast<Key &>(pk).key);
    element_set(key, const_cast<Key &>(pk).key);
    return(*this);
}

std::string Key::toString() const
{
    return Utils::pointToStr(key);
}

void Key::read(const std::string & str, const pairing_t & pairing)
    throw(ClException)
{
    (*this) = Key(str, pairing);
}

void Key::load(const std::string & filename, const pairing_t & pairing)
    throw(ClException)
{
    std::ifstream input(filename.c_str());
    std::string k;

    if (input.good())
    {
        input >> k;
        input.close();

        read(k, pairing);
    }
    else
    {
        throw ClException("Key::load: cannot open file");
    }
}

void Key::save(const std::string & filename)
{
    std::ofstream output(filename.c_str());

    output << toString() << std::endl;
    output.close();
}

std::ostream & operator<<(std::ostream & os, const Key & k)
{
    os << "Private Key" << std::endl << Utils::ident(k.toString());

    return os;
}
