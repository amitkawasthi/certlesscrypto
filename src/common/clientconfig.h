/*
 * Title:
 *     Client Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Client basic configuration
 *
 * NOTE:
 *     <note>
 */

#ifndef _CLIENTCONFIG_H_
#define _CLIENTCONFIG_H_

#include <string>
#include <iostream>
#include <fstream>
#include <socket_include.h>
#include "common/utils.h"

class ClientConfig
{
public:

    std::string filename;
    std::string addr;
    port_t port;

public:

    ClientConfig(const std::string & a_filename);

    /// Loads configuration from file
    void load() throw(ClException);

    /// Saves configuration
    void save();

    friend
    std::ostream & operator<<(std::ostream & os, const ClientConfig & cc);

};

#endif  // _CLIENTCONFIG_H_
