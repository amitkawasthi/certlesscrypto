/*
 * Title:
 *     Utilities
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Useful types and functions
 *
 * NOTE:
 *     <note>
 */

#ifndef _UTILS_H_
#define _UTILS_H_

#include <utility>
#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <cmath>
#include <openssl/rand.h>
#include <gmp.h>
#include <pbc.h>
#include "common/clexception.h"

typedef unsigned char Octet;
typedef std::vector<Octet> Octets;

typedef std::map<std::string, std::string> Arguments;

namespace
{
    /// Maximum length (in bits) of ECC element
    const size_t maxBitLen = 15360;
}

class Utils
{
public:

    /// Generates random octet string of given length
    static Octets generate(const size_t & len) throw(ClException);

    /// Converts character string to octet string
    static Octets strToOctets(const std::string & str);

    /// Converts octet string to character string
    static std::string octetsToStr(const Octets & oct);

    /// Performs xor operation on corresponding pair of octets
    static Octets xorOct(const Octets & a, const Octets & b) throw(ClException);

    /// Converts mpz to string
    static std::string mpzToStr(const mpz_t n, const unsigned long & base);

    /// Returns bit length of a number
    static size_t getBitLen(const mpz_t n);

    /// Converts element (not a point) to string
    static std::string elemToStr(const element_t & p);

    /// Converts point to string
    static std::string pointToStr(const element_t & pt);

    /// Sets point from a string
    static
    void strToPoint(element_t pt, const std::string & str) throw(ClException);

    /// Returns coordinates of a point
    static std::pair<std::string, std::string> getPoint(const std::string & pt);

    /// Parses single input parameter
    static std::pair<std::string, std::string> parseParam(char * arg);

    /// Parses input parameters
    static Arguments parseParams(int argc, char * argv[]);

    /// Sets a field from map
    template <typename T>
    static
    void setConfig(Arguments & arguments, const std::string & arg, T & val)
    {
        std::stringstream ss;

        if ("" != arguments[arg])
        {
            ss << arguments[arg];
            ss >> val;
        }
    }

    /// Idents string (without new line characters)
    static std::string ident(const std::string & str, size_t col = 4);

    /// Converts to string
    template <typename T>
    static std::string toString(const T & elem)
    {
        std::stringstream ss;

        ss << elem;
        return ss.str();
    }
};

#endif  // _UTILS_H_
