/*
 * Title:
 *     System Parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     System Parameters used by CL-PKE/IBE cryptosystem
 */

#include "common/systemparam.h"

SystemParam::SystemParam()
{
    init();
}

SystemParam::SystemParam(const size_t & n) throw(ClException)
    : pp(n)
{
    init();
    generate();
}

SystemParam::SystemParam(const PubParam & a_pp, const std::string & sStr)
    : pp(a_pp)
{
    init();
    element_set_str(s, const_cast<char*>(sStr.c_str()), 10);
}

SystemParam::SystemParam(const SystemParam & sp)
    : pp(sp.pp)
{
    init();
    element_set(s, const_cast<SystemParam&>(sp).s);
}

SystemParam::~SystemParam()
{
    element_clear(s);
}

SystemParam & SystemParam::operator=(const SystemParam & sp)
{
    pp = sp.pp;
    element_clear(s);
    init();
    element_set(s, const_cast<SystemParam&>(sp).s);
    return (*this);
}

void SystemParam::generate()
{
    element_random(s);
    element_pow_zn(pp.pPub, pp.p, s);
    pp.updateParams();
}

void SystemParam::init()
{
    element_init_Zr(s, pp.pairing);
}

std::string SystemParam::getS() const
{
    return Utils::elemToStr(s);
}

void SystemParam::load(const std::string & spf, const std::string & ppf)
throw(ClException)
{
    std::ifstream input(spf.c_str());
    std::string str;

    if (input.good())
    {
        input >> str;
        input.close();
    }
    else
    {
        throw ClException("SystemParam::load: Cannot open file");
    }
    try
    {
        pp.load(ppf);
    }
    catch (ClException e)
    {
        throw ClException("SystemParam::load:\n" + e.toString());
    }
    element_clear(s);
    init();
    element_set_str(s, const_cast<char*>(str.c_str()), 10);
}

void SystemParam::save(const std::string & spf, const std::string & ppf)
{
    std::ofstream output(spf.c_str());

    output << getS() << std::endl;
    output.close();
    pp.save(ppf);
}

std::ostream & operator<<(std::ostream & os, const SystemParam & sp)
{
    os << "Master secret" << std::endl << Utils::ident(sp.getS()) << sp.pp;
    return os;
}
