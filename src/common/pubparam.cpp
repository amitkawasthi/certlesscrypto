/*
 * Title:
 *     Public Parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Public Parameters used by CL-PKE/IBE cryptosystem
 */

#include "common/pubparam.h"

std::map<hashFcn, std::string> PubParam::initHashFunOidMap()
{
    std::map<hashFcn, std::string> map;

    map[HashFun::md2]    = "1.3.14.7.2.2.1";
    map[HashFun::md5]    = "1.2.840.113549.2.5";
    map[HashFun::sha1]   = "1.3.14.3.2.26";
    map[HashFun::sha224] = "2.16.840.1.101.3.4.2.4";
    map[HashFun::sha256] = "2.16.840.1.101.3.4.2.1";
    map[HashFun::sha384] = "2.16.840.1.101.3.4.2.2";
    map[HashFun::sha512] = "2.16.840.1.101.3.4.2.3";

    return map;
}

std::map<size_t, std::string> PubParam::initNOidMap()
{
    std::map<size_t, std::string> map;

    map[1024]  = "1.3.14.3.2.26";
    map[2048]  = "2.16.840.1.101.3.4.2.4";
    map[3072]  = "2.16.840.1.101.3.4.2.1";
    map[7680]  = "2.16.840.1.101.3.4.2.2";
    map[15360] = "2.16.840.1.101.3.4.2.3";

    return map;
}

std::map<size_t, size_t> PubParam::initLenLenMap()
{
    std::map<size_t, size_t> map;

    map[1024]  = 160;
    map[2048]  = 224;
    map[3072]  = 256;
    map[7680]  = 384;
    map[15360] = 512;

    return map;
}

std::map<hashFcn, std::string> PubParam::inithashFunNameMap()
{
    std::map<hashFcn, std::string> map;

    map[HashFun::md2]    = "MD2";
    map[HashFun::md5]    = "MD5";
    map[HashFun::sha1]   = "SHA1";
    map[HashFun::sha224] = "SHA224";
    map[HashFun::sha256] = "SHA256";
    map[HashFun::sha384] = "SHA384";
    map[HashFun::sha512] = "SHA512";

    return map;
}

PubParam::PubParam()
{
    init();
    setHashFcn(sha1Oid);
}

PubParam::PubParam(const size_t & n) throw(ClException)
    : cp(n / 2, getQBitLen(n))
    , kgcAddr("localhost")
    , kgcPort(6000)
{
    try
    {
        std::cout << "Generating public parameters...";
        init();
        setHashFcn(n);
        element_random(p);
        updateParams();
        std::cout << "done" << std::endl;
    }
    catch (ClException e)
    {
        clear();
        throw ClException("PubParam::PubParam:\n" + e.toString());
    }
}

PubParam::PubParam(const CurveParam & a_cp,
    const std::string & a_h,
    std::string & a_p,
    std::string & a_pPub,
    const std::string & a_kgcAddr,
    const port_t & a_kgcPort) throw(ClException)
    : cp(a_cp)
    , kgcAddr(a_kgcAddr)
    , kgcPort(a_kgcPort)
{
    try
    {
        init();
        Utils::strToPoint(p, a_p);
        Utils::strToPoint(pPub, a_pPub);
        setHashFcn(a_h);
        updateParams();
    }
    catch (ClException e)
    {
       clear();
       throw ClException("PubParam::PubParam:\n" + e.toString());
    }
}

PubParam::PubParam(const PubParam & pp)
    : cp(pp.cp)
    , h(pp.h)
    , kgcAddr(pp.kgcAddr)
    , kgcPort(pp.kgcPort)
{
    init();
    element_set(p, const_cast<PubParam&>(pp).p);
    element_set(pPub, const_cast<PubParam&>(pp).pPub);
    m_ppStr = pp.m_ppStr;
}

PubParam::~PubParam()
{
   clear();
}

PubParam & PubParam::operator=(const PubParam & pp)
{
    cp      = pp.cp;
    h       = pp.h;
    kgcAddr = pp.kgcAddr;
    kgcPort = pp.kgcPort;
    clear();
    init();
    element_set(p, const_cast<PubParam&>(pp).p);
    element_set(pPub, const_cast<PubParam&>(pp).pPub);
    m_ppStr = pp.m_ppStr;
    return (*this);
}

size_t PubParam::getHashLen() const
{
    return HashFun::getHashLen(h);
}

void PubParam::setHashFcn(const std::string & a_h) throw(ClException)
{
    std::map<hashFcn, std::string>::iterator it = hashFunOid.begin();

    for (; hashFunOid.end() != it && a_h != it -> second; ++it)
        ;

    if (hashFunOid.end() != it)
    {
        h = it -> first;
    }
    else
    {
        throw ClException("PubParam::setHashFcn: Unknown hash function");
    }
}

void PubParam::init()
{
    pairing_init_inp_buf(pairing,
        cp.getParams().c_str(),
        cp.getParams().size());
    element_init_G1(p, pairing);
    element_init_G1(pPub, pairing);
}

void PubParam::clear()
{
    element_clear(pPub);
    element_clear(p);
    pairing_clear(pairing);
}

void PubParam::setHashFcn(const unsigned long & n) throw(ClException)
{
    if ("" != nOid[n])
    {
        setHashFcn(nOid[n]);
    }
    else
    {
        throw ClException("PubParam::setHashFcn: Incorrect n");
    }
}

unsigned long PubParam::getQBitLen(const size_t & n) throw(ClException)
{
    if (0 != lenLen[n])
    {
        return lenLen[n];
    }
    else
    {
        throw ClException("PubParam::getQBitLen: Incorrect n");
    }
}

std::string PubParam::getP() const
{
    return Utils::pointToStr(p);
}

std::string PubParam::getPPub() const
{
    return Utils::pointToStr(pPub);
}

std::string PubParam::getHashFun() const throw(ClException)
{
    if ("" != hashFunOid[h])
    {
        return hashFunOid[h];
    }
    else
    {
        throw ClException("PubParam::getHashFun: Unknown hash function");
    }
}

std::string PubParam::getHashFunName() const throw(ClException)
{
    if ("" != hashFunName[h])
    {
        return hashFunName[h];
    }
    else
    {
        throw ClException("PubParam::getHashFunName: Unknown hash function");
    }
}

std::string PubParam::getPort() const
{
    return Utils::toString(kgcPort);
}

const std::string & PubParam::toString() const
{
    return m_ppStr;
}

void PubParam::read(const std::string & str) throw(ClException)
{
    std::stringstream ss(str);
    std::string ps;
    unsigned long exp2;
    unsigned long exp1;
    bool isNeg1;
    bool isNeg0;
    std::string a_HashFcn;
    std::string a_p;
    std::string a_pPub;
    std::string a_KgcAddr;
    port_t      a_KgcPort;

    ss >> ps
      >> exp2 >> exp1 >> isNeg1 >> isNeg0
      >> a_HashFcn
      >> a_p >> a_pPub
      >> a_KgcAddr >> a_KgcPort;

    if ("" != str)
    {
        try
        {
            cp.q = SolinasPrime(exp2, exp1, isNeg1, isNeg0);
            cp.setP(ps);
            clear();
            init();
            Utils::strToPoint(p, a_p);
            Utils::strToPoint(pPub, a_pPub);
            setHashFcn(a_HashFcn);
            kgcAddr = a_KgcAddr;
            kgcPort = a_KgcPort;
            updateParams();
        }
        catch (ClException e)
        {
            throw ClException("PubParam::read:\n" + e.toString());
        }
    }
    else
    {
        throw ClException("PubParam::read: empty parameters");
    }
}

void PubParam::load(const std::string & filename) throw(ClException)
{
    std::ifstream input(filename.c_str());
    std::string   output;

    while (input.good())
    {
        std::string tmp;
        input >> tmp;
        output.append(tmp);
        output.append(1, '\n');
    }

    try
    {
        read(output);
    }
    catch (ClException e)
    {
        throw ClException("PubParam::load:\n" + e.toString());
    }
}

void PubParam::save(const std::string & filename)
{
    std::ofstream output(filename.c_str());

    output << toString();
    output.close();
}

std::ostream & operator<<(std::ostream & os, const PubParam & pp)
{
    os << pp.cp
       << "Hash function OID (" << pp.getHashFunName() << ")" << std::endl
       << Utils::ident(pp.getHashFun())
       << "Point p" << std::endl
       << Utils::ident(pp.getP())
       << "Point pPub" << std::endl
       << Utils::ident(pp.getPPub())
       << "KGC address" << std::endl
       << Utils::ident(pp.kgcAddr)
       << "KGC port" << std::endl
       << Utils::ident(pp.getPort());
    return os;
}

void PubParam::updateParams()
{
    std::stringstream ss;

    ss << cp.getP() << std::endl
       << cp.q.exp2 << std::endl
       << cp.q.exp1 << std::endl
       << cp.q.isNeg1 << std::endl
       << cp.q.isNeg0 << std::endl
       << getHashFun() << std::endl
       << getP() << std::endl
       << getPPub() << std::endl
       << kgcAddr << std::endl
       << kgcPort << std::endl;

    m_ppStr = ss.str();
}
