/*
 * Title:
 *     Solinas Prime
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Representation and generation of Solinas primes
 */

#include "common/solinasprime.h"

SolinasPrime::SolinasPrime()
    : exp2(defExp2)
    , exp1(defExp1)
    , isNeg1(defIsNeg1)
    , isNeg0(defIsNeg0)
{
}

SolinasPrime::SolinasPrime(const unsigned long & a_exp2,
    const unsigned long & a_exp1,
    const bool a_isNeg1,
    const bool a_isNeg0)
    : exp2(a_exp2)
    , exp1(a_exp1)
    , isNeg1(a_isNeg1)
    , isNeg0(a_isNeg0)
{
}

std::string SolinasPrime::getParams() const
{
    return " r " + getPrime() +
        " exp2 " + getExp2() +
        " exp1 " + getExp1() +
        " sign1 " + getSign1() +
        " sign0 " + getSign0();
}

std::string SolinasPrime::getPrime() const
{
    mpz_t p;
    std::string str;

    mpz_init(p);
    compute(p);
    str = Utils::mpzToStr(p, 10);
    mpz_clear(p);
    return str;
}

std::string SolinasPrime::getExp2() const
{
    return getExp(exp2);
}

std::string SolinasPrime::getExp1() const
{
    return getExp(exp1);
}

std::string SolinasPrime::getSign1() const
{
    return getSign(isNeg1);
}

std::string SolinasPrime::getSign0() const
{
    return getSign(isNeg0);
}

std::string SolinasPrime::getExp(const unsigned long & exp)
{
    std::stringstream str;
    std::string result;

    str << exp;
    str >> result;
    return result;
}

std::string SolinasPrime::getSign(const bool isNeg)
{
    return isNeg ? "-1" : "1";
}

void SolinasPrime::addSigned2exp(mpz_t p, const bool isNeg,
    const unsigned long & exp)
{
    mpz_t tmp;

    mpz_init_set_ui(tmp, 1UL);
    mpz_mul_2exp(tmp, tmp, exp);
    if (isNeg)
    {
        mpz_neg(tmp, tmp);
    }
    mpz_add(p, p, tmp);
    mpz_clear(tmp);
}

void SolinasPrime::compute(mpz_t p) const
{
    mpz_set_ui(p, 0UL);
    addSigned2exp(p, false, exp2);
    addSigned2exp(p, isNeg1, exp1);
    addSigned2exp(p, isNeg0, 0UL);
}

std::ostream & operator<<(std::ostream & os, const SolinasPrime & q)
{
    os << "Solinas prime q" << std::endl
       << Utils::ident(q.getPrime())
       << Utils::ident("(" + q.getExp2() + ", " + q.getExp1() + ", "
           + q.getSign1() + ", " + q.getSign0() + ")");
    return os;
}
