/*
 * Title:
 *     Public Parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Public Parameters used by CL-PKE/IBE cryptosystem
 *
 * NOTE:
 *     <note>
 */

#ifndef _PUBPARAM_H_
#define _PUBPARAM_H_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <socket_include.h>
#include "common/hashfun.h"
#include "common/curveparam.h"
#include "common/clexception.h"

/// Class storing public parameters.
class PubParam
{
public:

    /// Elliptic Curve parameters
    CurveParam cp;

    /// Chosen (basic) hash function
    hashFcn h;

    /// Random generator of G_1 group
    element_t p;

    /// Public key
    element_t pPub;

    /// Used pairing
    pairing_t pairing;

    /// Key Generation Center address
    std::string kgcAddr;
    port_t      kgcPort;

private:

    /// String representation of Public Parameters
    std::string m_ppStr;

public:

    /// Initializes mapping between hash functions and corresponding OID
    static std::map<hashFcn, std::string> initHashFunOidMap();

    /// Initializes mapping between numbers and corresponding OID's
    static std::map<size_t, std::string> initNOidMap();

    /// Initializes mapping between lengths of keys
    static std::map<size_t, size_t> initLenLenMap();

    /// Initializes mapping between hash functions and corresponding names
    static std::map<hashFcn, std::string> inithashFunNameMap();

    PubParam();

    /**
     * Generates parameters corresponding to given security parameter
     * (RFC5091 5.1.2)
     */
    PubParam(const size_t & n) throw(ClException);

    /// Sets public parameters
    PubParam(const CurveParam & a_cp,
        const std::string & a_h,
        std::string & a_p,
        std::string & a_pPub,
        const std::string & a_kgcAddr,
        const port_t & a_kgcPort) throw(ClException);

    PubParam(const PubParam & pp);

    ~PubParam();

    PubParam & operator=(const PubParam & pp);

    /// Returns length of digest of selected hash function
    size_t getHashLen() const;

    /// Converts p point to string
    std::string getP() const;

    /// Converts pPub point to string
    std::string getPPub() const;

    /// Returns OID of hash function
    std::string getHashFun() const throw(ClException);

    /// Returns Name of hash function
    std::string getHashFunName() const throw(ClException);

    /// Returns port
    std::string getPort() const;

    /// Converts Public Parameters to string
    const std::string & toString() const;

    /// Reads Public Parameters from string
    void read(const std::string & str) throw(ClException);

    /// Loads configuration from file
    void load(const std::string & filename) throw(ClException);

    /// Saves configuration
    void save(const std::string & filename);

    friend
    std::ostream & operator<<(std::ostream & os, const PubParam & pp);

    void updateParams();

protected:

    /// Initializes pairing, p, pPub
    void init();

    /// Clears pairing, p, pPub
    void clear();

    /// Sets hash function according to OID description
    void setHashFcn(const std::string & a_h) throw(ClException);

    /// Sets hash function according to security parameter n
    void setHashFcn(const unsigned long & n) throw(ClException);

    /// Returns corresponding length of q
    static unsigned long getQBitLen(const size_t & n) throw(ClException);
};

namespace
{
    /// Hash functions OID's
    std::string md2Oid    = "1.3.14.7.2.2.1";
    std::string md5Oid    = "1.2.840.113549.2.5";
    std::string sha1Oid   = "1.3.14.3.2.26";
    std::string sha224Oid = "2.16.840.1.101.3.4.2.4";
    std::string sha256Oid = "2.16.840.1.101.3.4.2.1";
    std::string sha384Oid = "2.16.840.1.101.3.4.2.2";
    std::string sha512Oid = "2.16.840.1.101.3.4.2.3";

    /// Mapping between hash functions and corresponding OID
    std::map<hashFcn, std::string> hashFunOid(PubParam::initHashFunOidMap());

    /// Mapping between numbers and corresponding OID's
    std::map<size_t, std::string> nOid(PubParam::initNOidMap());

    /// Mapping between lengths of keys
    std::map<size_t, size_t> lenLen(PubParam::initLenLenMap());

    /// Mapping between hash functions and corresponding names
    std::map<hashFcn, std::string> hashFunName(PubParam::inithashFunNameMap());
}
#endif  // _PUBPARAM_H_
