/*
 * Title:
 *     Server Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     SSL Server basic configuration
 */

#include "common/serverconfig.h"

ServerConfig::ServerConfig(const std::string & a_filename)
    : filename(a_filename)
    , nThreads(1)
    , port(6000)
    , certFile("cert.pem")
    , certPass("haslo")
{
}

void ServerConfig::load() throw(ClException)
{
    std::ifstream input(filename.c_str());

    if (input.good())
    {
        input >> nThreads >> port >> certFile >> certPass;
        input.close();
    }
    else
    {
        throw ClException("ServerConfig::load: Cannot open file");
    }
}

void ServerConfig::save()
{
    std::ofstream output(filename.c_str());

    output << nThreads << std::endl
        << port << std::endl
        << certFile << std::endl
        << certPass << std::endl;
    output.close();
}

std::ostream & operator<<(std::ostream & os, const ServerConfig & sc)
{
    os << std::endl << "Server configuration" << std::endl
       << "Number of worker threads" << std::endl
       << Utils::ident(Utils::toString(sc.nThreads))
       << "Port" << std::endl
       << Utils::ident(Utils::toString(sc.port))
       << "Certificate file" << std::endl
       << Utils::ident(sc.certFile)
       << "Certificate password" << std::endl
       << Utils::ident(sc.certPass);

    return os;
}
