/*
 * Title:
 *     Ciphertext
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Ciphertext produced by CL-PKE/IBE cryptosystem
 */

#include "common/ciphertext.h"

Ciphertext::Ciphertext(const Key & a_u, const Octets & a_v, const Octets & a_w)
    : u(a_u), v(a_v), w(a_w)
{
}

Ciphertext::Ciphertext(const Ciphertext & c)
    : u(c.u), v(c.v), w(c.w)
{
}

Ciphertext::Ciphertext(const std::string & str, const pairing_t & pairing)
    throw(ClException)
    : u(pairing)
{
    std::stringstream ss(str);
    std::string uStr;
    std::string vStr;
    std::string wStr;
    Base64      base64;

    ss >> uStr >> vStr >> wStr;

    try
    {
        u = Key(uStr, pairing);
    }
    catch(ClException e)
    {
        throw ClException("Ciphertext::Ciphertext:\n" + e.toString());
    }

    v.resize(base64.decode_length(vStr));
    w.resize(base64.decode_length(wStr));
    size_t vLen = v.size();
    size_t wLen = w.size();
    base64.decode(vStr, &v[0], vLen);
    base64.decode(wStr, &w[0], wLen);
}

std::string Ciphertext::toString()
{
    Base64 base64;
    std::stringstream ss;
    std::string vStr;
    std::string wStr;

    base64.encode(&v[0], v.size(), vStr, false);
    base64.encode(&w[0], w.size(), wStr, false);

    ss << u.toString() << std::endl << vStr << std::endl << wStr;
    return ss.str();
}
