/*
 * Title:
 *     Semaphore
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     POSIX/Windows Semaphore wrapper
 *
 * NOTE:
 *     <note>
 */

#ifndef _CLSEMAPHORE_H_
#define _CLSEMAPHORE_H_

#include <string>
#ifdef _WIN32
#include <windows.h>
typedef HANDLE sem_t;
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#endif
#include "common/synchronization/threadexception.h"

/// Class representing semaphore
class Semaphore
{
private:

    sem_t m_sem;

public:

    Semaphore(int value) throw(ThreadException);

    ~Semaphore();

    /// proberen
    void p() throw(ThreadException);

    /// verhogen
    void v() throw(ThreadException);
};

#endif  // _CLSEMAPHORE_H_
