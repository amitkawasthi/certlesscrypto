/*
 * Title:
 *     Mutex
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     POSIX/Windows Mutex wrapper
 */

#include "common/synchronization/clmutex.h"

ClMutex::ClMutex() throw(ThreadException)
{
    bool isFailed = false;

#ifdef _WIN32
    m_mutex  = CreateMutex(NULL, FALSE, NULL);
    isFailed = NULL == m_mutex;
#else
    isFailed = 0 != pthread_mutex_init(&m_mutex, NULL);
#endif
    if (isFailed) throw ThreadException("Mutex init error");
}

ClMutex::~ClMutex()
{
#ifdef _WIN32
    WaitForSingleObject(m_mutex,INFINITE);
    CloseHandle(m_mutex);
#else
    pthread_mutex_lock(&m_mutex);
    pthread_mutex_unlock(&m_mutex);
    pthread_mutex_destroy(&m_mutex);
#endif
}

void ClMutex::lock() throw(ThreadException)
{
    bool isFailed = false;

#ifdef _WIN32
    isFailed = WAIT_FAILED == WaitForSingleObject(m_mutex, INFINITE);
#else
    isFailed = 0 != pthread_mutex_lock(&m_mutex);
#endif
    if (isFailed) throw ThreadException("Mutex lock error");
}

void ClMutex::unlock()
{
#ifdef _WIN32
    ReleaseMutex(m_mutex);
#else
    pthread_mutex_unlock(&m_mutex);
#endif
}
