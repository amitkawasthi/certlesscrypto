/*
 * Title:
 *     Monitor
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Monitor class
 */

#include "common/synchronization/monitor.h"

Monitor::Monitor() throw(ThreadException)
    : s(1)
{
}

void Monitor::enter() throw(ThreadException)
{
    s.p();
}

void Monitor::leave() throw(ThreadException)
{
    s.v();
}

void Monitor::wait(Condition & cond) throw(ThreadException)
{
    ++cond.waitingCount;
	leave();
	cond.wait();
}

void Monitor::signal(Condition & cond) throw(ThreadException)
{
    if(cond.signal())
    {
	    enter();
    }
}
