/*
 * Title:
 *     Key
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Key used by CL-PKE/IBE cryptosystem (as public, private, private partial
 *     keys)
 *
 * NOTE:
 *     <note>
 */

#ifndef _KEY_H_
#define _KEY_H_

#include "common/pubparam.h"

class Key
{
public:

    /// Key - a point
    element_t key;

public:

    Key(const pairing_t & pairing);

    Key(const std::string & str, const pairing_t & pairing) throw(ClException);

    Key(const element_t & dId, const element_t & x);

    Key(const Key & pk);

    ~Key();

    Key & operator=(const Key & k);

    std::string toString() const;

    void read(const std::string & str, const pairing_t & pairing)
        throw(ClException);

    void load(const std::string & filename, const pairing_t & pairing)
        throw(ClException);

    void save(const std::string & filename);

    friend std::ostream & operator<<(std::ostream & os, const Key & k);
};

#endif  // _KEY_H_
