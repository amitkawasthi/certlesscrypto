/*
 * Title:
 *     Hash functions
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Provies basic hash functions and more complicated functions for the
 *	   purpose of CL-PKE/IBE scheme
 */

#include "common/hashfun.h"

std::map<hashFcn, size_t> HashFun::initMap()
{
	std::map<hashFcn, size_t> map;

	map[md2]    = MD2_DIGEST_LENGTH;
	map[md5]    = MD5_DIGEST_LENGTH;
	map[sha1]   = SHA_DIGEST_LENGTH;
	map[sha224] = SHA224_DIGEST_LENGTH;
	map[sha256] = SHA256_DIGEST_LENGTH;
	map[sha384] = SHA384_DIGEST_LENGTH;
	map[sha512] = SHA512_DIGEST_LENGTH;

	return map;
}

Octets HashFun::md2(const Octets & d)
{
	return evalHash(MD2, hFunLen[md2], d);
}

Octets HashFun::md5(const Octets & d)
{
	return evalHash(MD5, hFunLen[md5], d);
}

Octets HashFun::sha1(const Octets & d)
{
	return evalHash(SHA1, hFunLen[sha1], d);
}

Octets HashFun::sha224(const Octets & d)
{
	return evalHash(SHA224, hFunLen[sha224], d);
}

Octets HashFun::sha256(const Octets & d)
{
	return evalHash(SHA256, hFunLen[sha256], d);
}

Octets HashFun::sha384(const Octets & d)
{
	return evalHash(SHA384, hFunLen[sha384], d);
}

Octets HashFun::sha512(const Octets & d)
{
	return evalHash(SHA512, hFunLen[sha512], d);
}

size_t HashFun::getHashLen(const hashFcn & f)
{
    return hFunLen[f];
}

/**
 * Converts parameters so that they are accepted by OpenSSL functions
 */
Octets HashFun::evalHash(fPtrHash h, const size_t & hashLen, const Octets & d)
{
	Octet * pHash = new Octet[hashLen];
	Octets hash;

	if (NULL != pHash && 0 != hashLen)
	{
		h(&d[0], d.size(), pHash);

		for (size_t i = 0; i < hashLen; hash.push_back(pHash[i++]))
			;

		delete[] pHash;
	}
	return hash;
}
#include<iostream>
void HashFun::hashToPoint(element_t q, const Octets & d, const hashFcn & h)
{
	Octets hash = h(d);

    element_from_hash(q, &hash[0], hash.size());
}

Octets HashFun::pointToOctets(element_t zeta, const size_t & n)
{
	Octets s(element_length_in_bytes(zeta));

	element_to_bytes(&s[0], zeta);
	s.resize(n / 4);
	return s;
}

void HashFun::rhoDataToRange(element_t l, const Octets & rho, const Octets & m,
	const hashFcn & h)
{
	hashToRange(l, concat(rho, h(m)), h);
}

Octets HashFun::hashBytes(const size_t & b, const Octets & p, const hashFcn & h)
{
	const size_t hashLen = hFunLen[h];
	Octets k = h(p);
	size_t l = (b + hashLen - 1) / hashLen;
	std::vector<Octets> hs;
	std::vector<Octets> rs;
	Octets r;

	hs.push_back(Octets(hashLen));

	for (size_t i = 1; i <= l; ++i)
	{
		hs.push_back(h(hs[i - 1]));
		rs.push_back(h(concat(hs.back(), k)));
		r.insert(r.end(), rs.back().begin(), rs.back().end());
	}

	r.resize(b);
	return r;
}

void HashFun::hashToRange(element_t v, const Octets & s, const hashFcn & h)
{
    mpz_t vTmp;

    mpz_init(vTmp);
    hashToInt(vTmp, s, h);

	element_set_mpz(v, vTmp);
	mpz_clear(vTmp);
}

void HashFun::hashToRange(mpz_t v, mpz_t n, const Octets & s, const hashFcn & h)
{
    hashToInt(v, s, h);
    mpz_mod(v, v, n);
}

void HashFun::hashToInt(mpz_t v, const Octets & s, const hashFcn & h)
{
	const size_t hashLen = hFunLen[h];
	Octets t(hashLen);

	mpz_set_ui(v, 0);

	for (size_t i = 1; i <= 2; ++i)
	{
		t = h(concat(t, s));
		computeV(v, hashLen, v, t);
	}
}

Octets HashFun::concat(const Octets & h, const Octets & s)
{
	Octets t(h);

	t.insert(t.end(), s.begin(), s.end());
	return t;
}

void HashFun::computeV(mpz_t v1, const size_t & hashLen, mpz_t v,
	const Octets & h)
{
	mpz_t a;

	mpz_init2(a, 8 * h.size());
	mpz_import(a, h.size(), 1, sizeof(Octet), 0, 0, &h[0]);

	mpz_mul_2exp(v1, v, 8 * static_cast<unsigned long>(hashLen));
	mpz_add(v1, v1, a);
	mpz_clear(a);
}
