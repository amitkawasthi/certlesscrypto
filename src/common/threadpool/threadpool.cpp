/*
 * Title:
 *     Thread pool
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     A pool of threads
 */

#include "common/threadpool/threadpool.h"

ThreadPool::ThreadPool(const size_t & nThreads) throw(ThreadException)
{
    for (size_t i = 0; i < nThreads; ++i)
    {
        m_workers.push_back(new WorkerThread(m_tasks));
        m_workers[i] -> run();
    }
}

ThreadPool::~ThreadPool()
{
    for (size_t i = 0; i < m_workers.size(); ++i)
    {
        delete m_workers[i];
    }
}

void ThreadPool::add(Task * task) throw(ThreadException)
{
    m_tasks.put(task);
}
