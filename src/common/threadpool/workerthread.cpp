/*
 * Title:
 *     Worker thread
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     A worker thread belonging to some pool
 */

#include "common/threadpool/workerthread.h"

WorkerThread::WorkerThread(SQueue<Task *> & squeue)
    : ClThread()
    , m_squeue(squeue)
{
}

void WorkerThread::main() throw(ThreadException)
{
    while (true)
    {
        Task * t = m_squeue.poll();
        t -> run();
        delete t;
    }
}
