/*
 * Title:
 *     Task
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     A task processed by thread pool
 *
 * NOTE:
 *     <note>
 */

#ifndef _TASK_H_
#define _TASK_H_

/// Base class for tasks
class Task
{
public:

    virtual ~Task()
    {
    }

    /// Computations processed by a thread
    virtual void run() = 0;
};

#endif  // _TASK_H_
