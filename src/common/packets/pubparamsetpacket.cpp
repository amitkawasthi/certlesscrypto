/*
 * Title:
 *     Set Public Parameters packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with CL-PKE/IBE public parameters
 */

#include "common/packets/pubparamsetpacket.h"

PubParamSetPacket::PubParamSetPacket(const PubParam & pubParam,
    const std::string & a_login, const std::string & a_pass)
    : pp(pubParam)
    , login(a_login)
    , pass(a_pass)
{
}

Octets PubParamSetPacket::serialize() const
{
    std::stringstream ss;

    ss << login << std::endl << pass << std::endl << pp.toString() << std::endl;
    return Utils::strToOctets(ss.str());
}

PubParamSetPacket
PubParamSetPacket::deserialize(const Octets & oct) throw(ClException)
{
    std::stringstream ss(Utils::octetsToStr(oct));
    std::string login;
    std::string pass;
    std::string rest;
    PubParam    pp;

    ss >> login >> pass;

    while (ss.good())
    {
        std::string tmp;
        ss >> tmp;
        rest.append(tmp);
        rest.append("\n");
    }

    pp.read(rest);
    return PubParamSetPacket(pp, login, pass);
}

PackType PubParamSetPacket::getType() const
{
    return PUBPARAM_SET_PACKET;
}

