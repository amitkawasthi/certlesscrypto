/*
 * Title:
 *     Key Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with ((partial) private) key
 */

#include "common/packets/keypacket.h"

KeyPacket::KeyPacket(const Key & a_k)
    : k(a_k)
{
}

Octets KeyPacket::serialize() const
{
    return Utils::strToOctets(k.toString());
}

KeyPacket KeyPacket::deserialize(const Octets & oct, const PubParam & pp)
    throw(ClException)
{
    return KeyPacket(Key(Utils::octetsToStr(oct), pp.pairing));
}

PackType KeyPacket::getType() const
{
    return KEY_PACKET;
}
