/*
 * Title:
 *     Public Parameters packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with CL-PKE/IBE encrypted message
 *
 * NOTE:
 *     <note>
 */

#ifndef _MSGPACKET_H_
#define _MSGPACKET_H_
#include <sstream>
#include "common/message.h"
#include "common/packets/packet.h"

class MsgPacket
    : public Packet
{
public:

    Message msg;

public:

    MsgPacket(const Message & m) : msg(m)
    {
    }

    Octets serialize() const
    {
        return Utils::strToOctets(msg.toString());
    }

    static MsgPacket deserialize(const Octets & oct)
    {
        return MsgPacket(Message(Utils::octetsToStr(oct)));
    }

    PackType getType() const
    {
        return MSG_PACKET;
    }
};

#endif  // _MSGPACKET_H_
