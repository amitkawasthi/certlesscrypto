/*
 * Title:
 *     Public Parameters packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with CL-PKE/IBE public parameters
 */

#include "common/packets/pubparampacket.h"

PubParamPacket::PubParamPacket(const PubParam & pubParam)
    : pp(pubParam)
{
}

Octets PubParamPacket::serialize() const
{
    return Utils::strToOctets(pp.toString());
}

PubParamPacket
PubParamPacket::deserialize(const Octets & oct) throw(ClException)
{
    PubParam pp;

    pp.read(Utils::octetsToStr(oct));
    return pp;
}

PackType PubParamPacket::getType() const
{
    return PUBPARAM_PACKET;
}
