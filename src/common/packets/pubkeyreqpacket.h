/*
 * Title:
 *     Public Key request packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet requests for sending public key
 *
 * NOTE:
 *     <note>
 */

#ifndef _PUBKEYREQPACKET_H_
#define _PUBKEYREQPACKET_H_

#include "common/utils.h"
#include "common/packets/packet.h"

class PubKeyReqPacket
    : public Packet
{
public:

    PubKeyReqPacket()
    {
    }

    Octets serialize() const
    {
        return Octets();
    }

    static PubKeyReqPacket deserialize()
    {
        return PubKeyReqPacket();
    }

    PackType getType() const
    {
        return PUBKEY_REQ_PACKET;
    }
};

#endif // _PUBKEYREQPACKET_H_
