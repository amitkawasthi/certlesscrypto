/*
 * Title:
 *     Key request packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet used for receiving (partial) private key
 *
 * NOTE:
 *     <note>
 */

#ifndef _KEYREQPACKET_H_
#define _KEYREQPACKET_H_
#include <sstream>
#include "common/packets/packet.h"

class KeyReqPacket
    : public Packet
{
public:

    std::string login;
    std::string pass;

public:

    KeyReqPacket(const std::string & a_login, const std::string & a_pass);

    Octets serialize() const;

    static KeyReqPacket deserialize(const Octets & oct);

    PackType getType() const;
};

#endif  // _KEYREQPACKET_H_
