/*
 * Title:
 *     Key Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with ((partial) private) key
 *
 * NOTE:
 *     <note>
 */

#ifndef _KEYPACKET_H_
#define _KEYPACKET_H_

#include <sstream>
#include "common/utils.h"
#include "common/key.h"
#include "common/packets/packet.h"

class KeyPacket
    : public Packet
{
public:

    const Key k;

public:

    KeyPacket(const Key & a_k);

    Octets serialize() const;

    static KeyPacket deserialize(const Octets & oct, const PubParam & pp)
        throw(ClException);

    PackType getType() const;
};

#endif /* _KEYPACKET_H_ */
