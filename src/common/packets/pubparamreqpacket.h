/*
 * Title:
 *     Public Parameters request packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet requests for sending public parameters
 *
 * NOTE:
 *     <note>
 */

#ifndef _PUBPARAMREQPACKET_H_
#define _PUBPARAMREQPACKET_H_

#include "common/utils.h"
#include "common/packets/packet.h"

class PubParamReqPacket
    : public Packet
{
public:

    PubParamReqPacket()
    {
    }

    Octets serialize() const
    {
        return Octets();
    }

    static PubParamReqPacket deserialize()
    {
        return PubParamReqPacket();
    }

    PackType getType() const
    {
        return PUBPARAM_REQ_PACKET;
    }
};

#endif // _PUBPARAMREQPACKET_H_
