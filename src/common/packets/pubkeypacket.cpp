/*/*
 * Title:
 *     Public Key Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet with public key
 */

#include "common/packets/pubkeypacket.h"

PubKeyPacket::PubKeyPacket(const PubKey & a_pk)
    : pk(a_pk)
{
}

Octets PubKeyPacket::serialize() const
{
    return Utils::strToOctets(pk.getXpub() + '\n' + pk.getYpub() + '\n');
}

PubKeyPacket PubKeyPacket::deserialize(const Octets & oct, const PubParam & pp)
    throw(ClException)
{
    std::stringstream ss(Utils::octetsToStr(oct));
    std::string x;
    std::string y;

    ss >> x >> y;
    return PubKeyPacket(PubKey(x, y, pp.pairing));
}

PackType PubKeyPacket::getType() const
{
    return PUBKEY_PACKET;
}
