/*
 * Title:
 *     Info Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet indicating error or success
 */

#include "common/packets/infopacket.h"

Octets InfoPacket::serialize() const
{
    Octets oct;
    InfoCode c = code;
    Octet * pCode = reinterpret_cast<Octet *>(&c);

    for (size_t i = 0; i < sizeof(InfoCode); ++i)
    {
        oct.push_back(*(pCode + i));
    }
    return oct;
}

InfoPacket InfoPacket::deserialize(const Octets & oct)
{
    InfoCode code;
    Octet * pCode = reinterpret_cast<Octet *>(&code);

    for (size_t i = 0; i < sizeof(InfoCode) && i < oct.size(); ++i)
    {
        *(pCode + i) = oct[i];
    }
    return InfoPacket(code);
}

