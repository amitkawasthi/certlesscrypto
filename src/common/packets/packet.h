/*
 * Title:
 *     Packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Base class for packets sent over the network
 *
 * NOTE:
 *     <note>
 */

#ifndef _PACKET_H_
#define _PACKET_H_
#include <string>
#include "common/utils.h"
#include "common/packets/packetexception.h"

enum PackType
{
    PUBPARAM_PACKET,
    PUBPARAM_REQ_PACKET,
    PUBPARAM_SET_PACKET,
    KEY_PACKET,
    KEY_REQ_PACKET,
    PUBKEY_PACKET,
    PUBKEY_REQ_PACKET,
    MSG_PACKET,
    ERROR_PACKET,
    INFO_PACKET
};

/// Serializable packet
class Packet
{
public:

    virtual ~Packet()
    {
    }

    /// Packet serialization
	virtual Octets serialize() const = 0;

	/// Returns type of packet
	virtual PackType getType() const = 0;
};

#endif  // _PACKET_H_
