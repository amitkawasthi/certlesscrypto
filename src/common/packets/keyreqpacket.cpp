/*
 * Title:
 *     Key request packet
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Packet used for receiving (partial) private key
 */

#include "common/packets/keyreqpacket.h"

KeyReqPacket::KeyReqPacket(const std::string & a_login,
    const std::string & a_pass)
    : login(a_login)
    , pass(a_pass)
{
}

Octets KeyReqPacket::serialize() const
{
    std::stringstream ss;

    ss << login << std::endl << pass << std::endl;
    return Utils::strToOctets(ss.str());
}

KeyReqPacket KeyReqPacket::deserialize(const Octets & oct)
{
    std::stringstream ss(Utils::octetsToStr(oct));
    std::string login;
    std::string pass;

    ss >> login >> pass;
    return KeyReqPacket(login, pass);
}

PackType KeyReqPacket::getType() const
{
    return KEY_REQ_PACKET;
}
