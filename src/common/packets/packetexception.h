/*
 * Title:
 *     Packet exception
 *
 * Author:
 *     Kacper Bak
 *
 * Description:
 *     Indicates some error with packets/communication/xml
 *
 * NOTE:
 *     <none>
 */

#ifndef _PACKETEXCEPTION_H_
#define _PACKETEXCEPTION_H_

#include "common/clexception.h"

class PacketException
    : public ClException
{
public:

    PacketException(const std::string & error) : ClException(error)
    {
    }
};

#endif // _PACKETEXCEPTION_H_
