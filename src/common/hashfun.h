/*
 * Title:
 *     Hash functions
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Provies basic hash functions and more complicated functions for the
 *	   purpose of CL-PKE/IBE scheme
 *
 * NOTE:
 *     <note>
 */

#ifndef _HASHFUN_H_
#define _HASHFUN_H_

#include <math.h>
#include <map>
#include <vector>
#include <string>
#include <openssl/md2.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <gmp.h>
#include <pbc.h>
#include "common/utils.h"

typedef Octet* (*fPtrHash)(const Octet * d, size_t n, Octet * md);
typedef Octets (*hashFcn)(const Octets & d);

/// Class containing static hash methods.
class HashFun
{
public:

	/// Initializes map of hash functions and lengths of output digests
	static std::map<hashFcn, size_t> initMap();

	/// Sample hash functions
	static Octets md2(const Octets & d);
	static Octets md5(const Octets & d);
	static Octets sha1(const Octets & d);
	static Octets sha224(const Octets & d);
	static Octets sha256(const Octets & d);
	static Octets sha384(const Octets & d);
	static Octets sha512(const Octets & d);

    /// Returns length of digest for given function
    static size_t getHashLen(const hashFcn & f);

    /**
	 * Maps string onto point on elliptic curve (y^2 = x^3 + x)
	 * H1: {0, 1}* -> G1*
	 */
    static void hashToPoint(element_t q, const Octets & d, const hashFcn & h);

	/**
	 * Maps point to string
	 * H2: G2 -> {0, 1)^n
	 */
	static Octets pointToOctets(element_t zeta, const size_t & n);

	/**
	 * Maps concatenated rho and m octets onto range <0, q - 1>
	 * H3: {0, 1}^n x {0, 1}^n -> Z_q*
	 */
	static void rhoDataToRange(element_t l, const Octets & rho,
		const Octets & m, const hashFcn & h);

	/**
	 * Keyed cryptographic pseudo-random bytes generator
	 * H4: {0, 1}^n -> {0, 1}^n
	 * RFC5091 4.2.1 HashBytes
	 */
	static
	Octets hashBytes(const size_t & b, const Octets & p, const hashFcn & h);

	/// Hashes string to range of v element using provided hash function
	static void hashToRange(element_t v, const Octets & s, const hashFcn & h);

    /**
	 * Hashes string onto range <0, n - 1> using provided hash function
	 * RFC5091 4.1.1 HashToRange
	 */
    static
    void hashToRange(mpz_t v, mpz_t n, const Octets & s, const hashFcn & h);

    /// Hashes string onto integer
    static void hashToInt(mpz_t v, const Octets & s, const hashFcn & h);

protected:

	/// Computes given hash function (wraps usage of OpenSSL)
	static
	Octets evalHash(fPtrHash h, const size_t & hashLen, const Octets & d);

private:

	/// Concatenates octet strings: h || s
	static Octets concat(const Octets & h, const Octets & s);

	/// Computes the following formula: 256^hashLen * v + value(h)
	static
	void computeV(mpz_t v1, const size_t & hashLen, mpz_t v, const Octets & h);
};

namespace
{
	/// Mapping between hash functions and lengths of output digests
	std::map<hashFcn, size_t> hFunLen(HashFun::initMap());
}

#endif  // _HASHFUN_H_
