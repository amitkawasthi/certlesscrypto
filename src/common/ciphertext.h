/*
 * Title:
 *     Ciphertext
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Ciphertext produced by CL-PKE/IBE cryptosystem
 *
 * NOTE:
 *     <note>
 */

#ifndef _CIPHERTEXT_H_
#define _CIPHERTEXT_H_

#include <string>
#include <sstream>
#include <Base64.h>
#include "common/utils.h"
#include "common/key.h"

/// Class containing ciphertext.
class Ciphertext
{
public:

    /// Ciphertext
    Key    u;
    Octets v;
    Octets w;

public:

    Ciphertext(const Key & a_u, const Octets & a_v, const Octets & a_w);

    Ciphertext(const Ciphertext & c);

    Ciphertext(const std::string & str, const pairing_t & pairing)
        throw(ClException);

    std::string toString();
};

#endif  // _CIPHERTEXT_H_
