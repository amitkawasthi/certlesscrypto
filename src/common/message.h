/*
 * Title:
 *     Message
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Message encrypted with CEK (Content Encryption Key)
 *
 * NOTE:
 *     <note>
 */

#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <openssl/aes.h>
#include <openssl/evp.h>
#include <Base64.h>
#include <socket_include.h>
#include "common/utils.h"
#include "common/clalgorithms.h"

namespace
{
    const size_t blockSize = 128 / 8;
    const size_t keyLength = 256 / 8;
}

class Message
{
public:

    Octets      msg;
    std::string id;

    /// Content Encryption Key (key + initial vector)
    Octets      cek;

    /// PPS address and port
    std::string addr;
    port_t      port;

public:

    Message();

    Message(const std::string & a_msg, const std::string & a_id,
        const Octets & a_cek, const std::string & a_addr,
        const port_t & a_port);

    Message(const std::string & str);

    void genCek();

    void encryptMsg();

    void decryptMsg();

    Octets doCryptMsg(const bool & doEncrypt);

    void encryptCek(const PubParam & pp, const PubKey & pk) throw(ClException);

    void decryptCek(const Key & pk, const PubParam & pp) throw (ClException);

    std::string toString() const;
};

#endif  // _MESSAGE_H_
