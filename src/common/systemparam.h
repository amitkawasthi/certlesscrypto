/*
 * Title:
 *     System Parameters
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     System Parameters used by CL-PKE/IBE cryptosystem
 *
 * NOTE:
 *     <note>
 */

#ifndef _SYSTEMPARAM_H_
#define _SYSTEMPARAM_H_

#include <string>
#include <iostream>
#include "common/pubparam.h"
#include "common/clexception.h"

/// Class storing system parameters.
class SystemParam
{
public:

    /// Public parameters
    PubParam pp;

    /// Master secret
    element_t s;

public:

    SystemParam();

    /**
     * Generates parameters corresponding to given security parameter
     * (RFC5091 5.1.2)
     */
    SystemParam(const size_t & n) throw(ClException);

    /// Sets system parameters
    SystemParam(const PubParam & a_pp, const std::string & sStr);

    SystemParam(const SystemParam & sp);

    ~SystemParam();

    SystemParam & operator=(const SystemParam & sp);

    /// Generates a pair of keys (Public (pp.pPub) and Master secret (s))
    void generate();

    std::string getS() const;

    /// Loads configuration from files
    void
    load(const std::string & spf, const std::string & ppf) throw(ClException);

    /// Saves configuration
    void save(const std::string & spf, const std::string & ppf);

    friend
    std::ostream & operator<<(std::ostream & os, const SystemParam & sp);

protected:

    /// Initializes master secret
    void init();
};

#endif  // _SYSTEMPARAM_H_
