/*
 * Title:
 *     Client Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Client basic configuration
 */

#include "common/clientconfig.h"

ClientConfig::ClientConfig(const std::string & a_filename)
    : filename(a_filename)
    , addr("localhost")
    , port(80)
{
}

void ClientConfig::load() throw(ClException)
{
    std::ifstream input(filename.c_str());

    if (input.good())
    {
        input >> addr >> port;
        input.close();
    }
    else
    {
        throw ClException("ClientConfig::load: Cannot open file");
    }
}

void ClientConfig::save()
{
    std::ofstream output(filename.c_str());

    output << addr << std::endl << port << std::endl;
    output.close();
}

std::ostream & operator<<(std::ostream & os, const ClientConfig & cc)
{
    os << "Client configuration" << std::endl
       << "Address" << std::endl
       << Utils::ident(cc.addr)
       << "Port" << std::endl
       << Utils::ident(Utils::toString(cc.port));

    return os;
}
