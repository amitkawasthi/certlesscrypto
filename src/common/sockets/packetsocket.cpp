/*
 * Title:
 *     Packet Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Socket capable of using packets
 */

#include "common/sockets/packetsocket.h"

PacketSocket::PacketSocket(ISocketHandler & h)
    : TcpSocket(h)
{
}

PacketHandler & PacketSocket::getPacketHandler() const
{
    return static_cast<PacketHandler &>(MasterHandler());
}

void PacketSocket::OnRawData(const char * buf, size_t len)
{
    if (0 < len)
    {
        Octets input;

        if (1 < len)
        {
            input.insert(input.end(), buf + 1, buf + len);
        }

        switch (buf[0])
        {
        case PUBPARAM_PACKET:
            onPubParamPacket(PubParamPacket::deserialize(input));
            break;
        case PUBPARAM_REQ_PACKET:
            onPubParamReqPacket(PubParamReqPacket::deserialize());
            break;
        case PUBPARAM_SET_PACKET:
            onPubParamSetPacket(PubParamSetPacket::deserialize(input));
            break;
        case KEY_PACKET:
            onKeyPacket(KeyPacket::deserialize(input, getPacketHandler().pp));
            break;
        case KEY_REQ_PACKET:
            onKeyReqPacket(KeyReqPacket::deserialize(input));
            break;
        case PUBKEY_PACKET:
            onPubKeyPacket(PubKeyPacket::deserialize(input, getPacketHandler().pp));
            break;
        case PUBKEY_REQ_PACKET:
            onPubKeyReqPacket(PubKeyReqPacket::deserialize());
            break;
        case MSG_PACKET:
            onMsgPacket(MsgPacket::deserialize(input));
            break;
        case ERROR_PACKET:
            onErrorPacket(ErrorPacket::deserialize(input));
            break;
        case INFO_PACKET:
            onInfoPacket(InfoPacket::deserialize(input));
            break;
        default:
            std::cerr << "PacketSocket::OnRawData: unrecognized packet"
                << std::endl;
            send(InfoPacket(PACKET_UNRECOGNIZED));
        }
    }
}

void PacketSocket::onPubParamPacket(const PubParamPacket & packet)
{
    std::cerr << "PacketSocket::onPubParamPacket: unrecognized packet"
        << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onPubParamReqPacket(const PubParamReqPacket & packet)
{
    std::cerr << "PacketSocket::onPubParamReqPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onPubParamSetPacket(const PubParamSetPacket & packet)
{
    std::cerr << "PacketSocket::onPubParamSetPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onKeyPacket(const KeyPacket & packet)
{
    std::cerr << "PacketSocket::onKeyPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onKeyReqPacket(const KeyReqPacket & packet)
{
    std::cerr << "PacketSocket::onKeyReqPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onPubKeyPacket(const PubKeyPacket & packet)
{
    std::cerr << "PacketSocket::onPubKeyPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onPubKeyReqPacket(const PubKeyReqPacket & packet)
{
    std::cerr << "PacketSocket::onPubKeyReqPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onMsgPacket(const MsgPacket & packet)
{
    std::cerr << "PacketSocket::onMsgPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onErrorPacket(const ErrorPacket & packet)
{
    std::cerr << "PacketSocket::onErrorPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::onInfoPacket(const InfoPacket & packet)
{
    std::cerr << "PacketSocket::onInfoPacket: unrecognized packet"
            << std::endl;
    send(InfoPacket(PACKET_UNRECOGNIZED));
}

void PacketSocket::send(const Packet & packet)
{
    Octets output(packet.serialize());

    output.insert(output.begin(), packet.getType());
    Send(Utils::octetsToStr(output));
}
