/*
 * Title:
 *     SSL Server Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Base class for SSL server sockets
 *
 * NOTE:
 *     <note>
 */

#ifndef _SSLSERVERSOCKET_H_
#define _SSLSERVERSOCKET_H_

#include "common/sockets/packetsocket.h"
#include "common/sockets/sslhandler.h"

class SslServerSocket
    : public PacketSocket
{
public:

    SslServerSocket(ISocketHandler & h);

    void Init();

    void InitSSLServer();

    SslHandler & getSslHandler() const;
};

#endif // _SSLSERVERSOCKET_H_
