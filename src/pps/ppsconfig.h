/*
 * Title:
 *     PPS Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Configuration of PPS
 *
 * NOTE:
 *     <note>
 */

#ifndef _PPSCONFIG_H_
#define _PPSCONFIG_H_

#include "common/serverconfig.h"
#include "common/pubparam.h"
#include "common/clexception.h"

namespace
{
    const std::string servConf    = ".ppsservconfig";
    const std::string ppsPpConf   = ".ppsppconfig";
    const std::string ppsAuthConf = ".ppsauthconfig";
}

class PpsConfig
{
public:

    ServerConfig sc;
    std::string login;
    std::string pass;

private:

    PubParam * m_pp;

public:

    PpsConfig();

    ~PpsConfig();

    /// Loads configuration from file
    void load() throw(ClException);

    /// Saves configuration
    void save() throw(ClException);

    PubParam & getPubParam() const throw(ClException);

    void setPubParam(const PubParam & pp);

    friend std::ostream & operator<<(std::ostream & os, const PpsConfig & c);
};

#endif  // _PPSCONFIG_H_
