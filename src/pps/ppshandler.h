/*
 * Title:
 *     PPS Socket Handler
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Handles PPS sockets
 *
 * NOTE:
 *     <note>
 */

#ifndef _PPSHANDLER_H_
#define _PPSHANDLER_H_

#include "common/sockets/sslhandler.h"
#include "pps/ppsconfig.h"

class PpsHandler
    : public SslHandler
{
public:

    PpsConfig & conf;

public:

    PpsHandler(Mutex & m, PpsConfig & c)
        : SslHandler(m, c.getPubParam(), c.sc)
        , conf(c)
     {
     }
};

#endif  // _PPSANDLER_H_
