/*
 * Title:
 *     PPS Server Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     PPS server socket
 *
 * NOTE:
 *     <note>
 */

#ifndef _PPSSOCKET_H_
#define _PPSSOCKET_H_

#include "common/utils.h"
#include "common/packets/packet.h"
#include "common/packets/pubparampacket.h"
#include "common/packets/errorpacket.h"
#include "common/sockets/sslserversocket.h"
#include "pps/ppshandler.h"

class PpsServerSocket
    : public SslServerSocket
{
public:

    PpsServerSocket(ISocketHandler & h);

    PpsHandler & getPpsHandler() const;

    void onPubParamReqPacket(const PubParamReqPacket & packet);

    void onPubParamSetPacket(const PubParamSetPacket & packet);
};

#endif  // _PPSSOCKET_H_
