/*
 * Title:
 *     CL-PKE test
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Tests for CL-PKE scheme
 *
 * NOTE:
 *     <note>
 */

#ifndef _CIPKETEST_H_
#define _CIPKETEST_H_

#include <iostream>
#include "common/hashfun.h"
#include "common/clalgorithms.h"

void testClPke();
void testClPkeIbe();

#endif  // _CIPKETEST_H_
