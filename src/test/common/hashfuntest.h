/*
 * Title:
 *     HashFun test
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Tests for hash functions
 *
 * NOTE:
 *     <note>
 */

#ifndef _HASHFUNTEST_H_
#define _HASHFUNTEST_H_

#include <iostream>
#include "common/hashfun.h"
#include "common/clalgorithms.h"

void testHashToRange();

#endif  // _HASHFUNTEST_H_
