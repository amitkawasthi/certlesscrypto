/*
 * Title:
 *     Message test
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Tests for message encryption/decryption
 *
 * NOTE:
 *     <note>
 */

#ifndef _MESSAGETEST_H_
#define _MESSAGETEST_H_

#include <iostream>
#include "common/message.h"

void testMessageCrypt();
void testMessageBase64();

#endif  // _MESSAGETEST_H_
