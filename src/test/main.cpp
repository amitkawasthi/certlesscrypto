/*
 * Title:
 *     Tests
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Main function for running all tests
 *
 * NOTE:
 *     <note>
 */

#include "test/common/hashfuntest.h"
#include "test/common/clpketest.h"
#include "test/common/messagetest.h"

int main()
{
    testHashToRange();
    testClPke();
    testClPkeIbe();
    testMessageCrypt();
    testMessageBase64();
    return 0;
}
