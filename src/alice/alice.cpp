/*
 * Title:
 *     Alice
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Alice - sender
 *
 * NOTE:
 *     <note>
 */

#include <ListenSocket.h>
#include "common/utils.h"
#include "common/sockets/ppsclientsocket.h"
#include "alice/aliceclientsocket.h"
#include "alice/aliceconfig.h"

/// Runs Alice's client
void run(Arguments & arguments, AliceConfig & aliceConfig)
{
    std::cout << "Downloading Public Parameters... ";

    try
    {
        PubParam pp;
        PacketHandler h(pp);
        PpsClientSocket ppsSock(h);

        ppsSock.Open(aliceConfig.pps.addr, aliceConfig.pps.port);
        h.Add(&ppsSock);
        while (h.GetCount())
        {
            h.Select();
        }
        std::cout << pp << std::endl;

        AliceClientSocket aliceSock(h, aliceConfig);

        aliceSock.Open(aliceConfig.bob.addr, aliceConfig.bob.port);
        h.Add(&aliceSock);
        while (h.GetCount())
        {
            h.Select();
        }
    }
    catch (ClException e)
    {
        e.print();
    }
}

int main(int argc, char * argv[])
{
    Arguments arguments = Utils::parseParams(argc, argv);
    AliceConfig aliceConfig;

    try
    {
        Utils::setConfig(arguments, "ppsaddr", aliceConfig.pps.addr);
        Utils::setConfig(arguments, "ppsport", aliceConfig.pps.port);
        Utils::setConfig(arguments, "bobaddr", aliceConfig.bob.addr);
        Utils::setConfig(arguments, "bobport", aliceConfig.bob.port);
        run(arguments, aliceConfig);
    }
    catch (ClException e)
    {
        e.print();
    }
    return 0;
}
