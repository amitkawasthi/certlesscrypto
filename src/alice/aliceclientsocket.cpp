/*
 * Title:
 *     Alice Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Used by Alice to send a message to Bob
 */

#include "alice/aliceclientsocket.h"

AliceClientSocket::AliceClientSocket(ISocketHandler & h, AliceConfig & c)
    : PacketSocket(h)
    , m_aliceConfig(c)
{
}

void AliceClientSocket::OnConnect()
{
    send(PubKeyReqPacket());
    std::cout << "Key request pending... ";
}

void AliceClientSocket::OnConnectFailed()
{
    std::cout << "failed\n[error] Cannot connect to Bob" <<std::endl;
}

void AliceClientSocket::onPubKeyPacket(const PubKeyPacket & packet)
    throw(ClException)
{
    std::string msg;
    std::string tmp;

    std::cout << "done" << std::endl << std::endl << "Enter ID\n    ";
    std::cin >> m_aliceConfig.m.id;
    std::cout << "Message\n    ";

    std::cin.ignore();
    do
    {
        std::getline(std::cin, tmp);
        msg.append(tmp);
    } while (!tmp.empty());

    m_aliceConfig.m.addr = m_aliceConfig.pps.addr;
    m_aliceConfig.m.port = m_aliceConfig.pps.port;
    m_aliceConfig.m.msg  = Utils::strToOctets(msg);

    try
    {
        m_aliceConfig.m.genCek();
        m_aliceConfig.m.encryptMsg();
        m_aliceConfig.m.encryptCek(getPacketHandler().pp, packet.pk);
        send(MsgPacket(m_aliceConfig.m));
    }
    catch (ClException e)
    {
        e.print();
    }
    SetCloseAndDelete();
}
