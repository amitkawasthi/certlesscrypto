/*
 * Title:
 *     Alice Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Used by Alice to send a message to Bob
 *
 * NOTE:
 *     <note>
 */

#ifndef _ALICECLIENTSOCKET_H_
#define _ALICECLIENTSOCKET_H_

#include <iostream>
#include <string>
#include "common/clalgorithms.h"
#include "common/sockets/packetsocket.h"
#include "alice/aliceconfig.h"

class AliceClientSocket
    : public PacketSocket
{
protected:

    AliceConfig & m_aliceConfig;

public:

    AliceClientSocket(ISocketHandler & h, AliceConfig & c);

    void OnConnect();

    void OnConnectFailed();

    void onPubKeyPacket(const PubKeyPacket & packet) throw(ClException);
};

#endif // _ALICECLIENTSOCKET_H_
