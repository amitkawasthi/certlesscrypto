/*
 * Title:
 *     Alice Configuration
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Configuration of Alice
 *
 * NOTE:
 *     <note>
 */

#ifndef _ALICECONFIG_H_
#define _ALICECONFIG_H_

#include "common/clientconfig.h"
#include "common/message.h"

class AliceConfig
{
public:

    /// PPS connection
    ClientConfig pps;

    // Bob Connection
    ClientConfig bob;

    Message m;

public:

    AliceConfig()
        : pps("")
        , bob("")
    {
        pps.addr = "localhost";
        pps.port = 6001;
        bob.addr = "localhost";
        bob.port = 6002;
        m.msg    = Utils::strToOctets("Hello! CL-PKE is cool!");
    }
};

#endif  // _ALICECONFIG_H_
