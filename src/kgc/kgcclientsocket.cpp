/*
 * Title:
 *     KGC Client Socket
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Used by KGC to connect with PPS
 */

#include "kgc/kgcclientsocket.h"

KgcClientSocket::KgcClientSocket(ISocketHandler & h,
    const std::string & login, const std::string & pass)
    : SslClientSocket(h)
    , m_login(login)
    , m_pass(pass)
{
}

void KgcClientSocket::OnConnect()
{
    send(PubParamSetPacket(getPacketHandler().pp, m_login, m_pass));
}

void KgcClientSocket::OnConnectFailed()
{
    std::cout << "failed\n[error] Cannot connect to PPS" <<std::endl;
}

void KgcClientSocket::onInfoPacket(const InfoPacket & packet)
{
    std::string msg;

    switch (packet.code)
    {
    case PACKET_SUCCESS:
        msg = "done";
        break;
    case PACKET_AUTH_FAILED:
        msg = "failed\n[error] Authorization failed. \
            Check your login & password";
        break;
    case PACKET_INVALID:
        msg = "failed\n[error] Invalid Public Parameters";
        break;
    default:
        msg = "failed\n[error] Unexpected error";
    }

    std::cout << msg << std::endl;
    SetCloseAndDelete();
}
