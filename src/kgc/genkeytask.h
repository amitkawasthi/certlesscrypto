/*
 * Title:
 *     Generate Key Task
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Generates (partial) private key from given ID
 *
 * NOTE:
 *     <note>
 */

#ifndef _GENKEYTASK_H_
#define _GENKEYTASK_H_
#include <string>
#include <sstream>
#include <Lock.h>
#include <SocketHandler.h>
#include <TcpSocket.h>
#include "common/threadpool/task.h"
#include "common/utils.h"
#include "common/clalgorithms.h"
#include "common/systemparam.h"
#include "common/packets/keypacket.h"
#include "kgc/kgcserversocket.h"

class GenKeyTask
    : public Task
{
private:

    KgcServerSocket * m_socket;
    SocketHandler & m_h;
    const SystemParam & m_sp;
    std::string m_id;

public:

    GenKeyTask(KgcServerSocket * socket, SocketHandler & h,
        const SystemParam & sp, const std::string & id);

    /// Generates key and sends to the user
    void run();
};

#endif  // _GENKEYTASK_H_
