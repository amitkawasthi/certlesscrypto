/*
 * Title:
 *     KGC Socket Handler
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Handles KGC sockets
 *
 * NOTE:
 *     <note>
 */

#ifndef _KGCHANDLER_H_
#define _KGCHANDLER_H_

#include "common/sockets/sslhandler.h"
#include "kgc/kgcconfig.h"

class KgcHandler
    : public SslHandler
{
public:

    KgcConfig & conf;

public:

    KgcHandler(Mutex & m, KgcConfig & c)
        : SslHandler(m, c.getSystemParam().pp, c.sc)
        , conf(c)
     {
     }
};

#endif  // _KGCHANDLER_H_
