/*
 * Title:
 *     KGC
 *
 * Author:
 *     Kacper Bak, <k(dot)bak(dot)1@stud.elka.pw.edu.pl>
 *
 * Description:
 *     Key Generation Center
 *
 * NOTE:
 *     <note>
 */

#include <Mutex.h>
#include <ListenSocket.h>
#include <Lock.h>
#include <Utility.h>
#include "common/utils.h"
#include "common/clalgorithms.h"
#include "kgc/kgchandler.h"
#include "kgc/kgcserversocket.h"
#include "kgc/kgcclientsocket.h"
#include "kgc/kgcconfig.h"

/// Sets up KGC (Key Generation Center) and sends Public Parameters to PPS
void setup(Arguments & arguments)
{
    KgcConfig kgcConfig;
    std::stringstream ss;
    unsigned long n = 0;

    ss << arguments["setup"];
    ss >> n;

    try
    {
        std::cout << "Generating system parameters..." << std::endl;
        kgcConfig.setSystemParam(ClAlgorithms::setup(n));

        Utils::setConfig(arguments, "nthreads", kgcConfig.sc.nThreads);
        Utils::setConfig(arguments, "port", kgcConfig.sc.port);
        Utils::setConfig(arguments, "ppsport", kgcConfig.cc.port);
        Utils::setConfig(arguments, "ppsaddr", kgcConfig.cc.addr);
        Utils::setConfig(arguments, "ppslogin", kgcConfig.ppsLogin);
        Utils::setConfig(arguments, "ppspass", kgcConfig.ppsPass);
        Utils::setConfig(arguments, "certfile", kgcConfig.sc.certFile);
        Utils::setConfig(arguments, "certpass", kgcConfig.sc.certPass);

        kgcConfig.getSystemParam().pp.kgcAddr = Utility::GetLocalAddress();
        kgcConfig.getSystemParam().pp.kgcPort = kgcConfig.sc.port;

        std::cout << kgcConfig;
        kgcConfig.save();

        std::cout << "Setting up PPS... ";

        PacketHandler h(kgcConfig.getSystemParam().pp);
        KgcClientSocket sock(h, kgcConfig.ppsLogin, kgcConfig.ppsPass);

        sock.Open(kgcConfig.cc.addr, kgcConfig.cc.port);
        h.Add(&sock);
        while (h.GetCount())
        {
            h.Select();
        }
    }
    catch (ClException e)
    {
        e.print();
    }
}

/// Runs KGC server
void run(Arguments & arguments, KgcConfig & kgcConfig)
{
    Mutex lock;
    KgcHandler h(lock, kgcConfig);
    ListenSocket<KgcServerSocket> l(h);

    if (l.Bind(kgcConfig.sc.port))
    {
        std::cerr << "Bind() failed" << std::endl;
        exit(-1);
    }
    Utility::ResolveLocal();
    h.Add(&l);
    while (true)
    {
        h.Select();
    }
}

int main(int argc, char * argv[])
{
    Arguments arguments = Utils::parseParams(argc, argv);
    KgcConfig kgcConfig;

    if (arguments.end() != arguments.find("setup"))
    {
        setup(arguments);
    }
    else
    {
        try
        {
            kgcConfig.load();
            Utils::setConfig(arguments, "nthreads", kgcConfig.sc.nThreads);
            Utils::setConfig(arguments, "certfile", kgcConfig.sc.certFile);
            Utils::setConfig(arguments, "certpass", kgcConfig.sc.certPass);
            std::cout << kgcConfig;
            run(arguments, kgcConfig);
        }
        catch (ClException e)
        {
            e.print();
        }
    }
    return 0;
}

