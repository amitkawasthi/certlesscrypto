import Prelude hiding ( (>>) )
import Numeric
import Test.QuickCheck

f_p :: Integer
f_p = 0xbffffffffffffffffffffffffffcffff3

newtype IntZp = N Integer deriving (Eq, Show)

instance Num IntZp where
  N a + N b = N ((a + b) `mod` f_p)
  N a - N b = N ((a - b) `mod` f_p)
  N a * N b = N ((a * b) `mod` f_p)

instance Fractional IntZp where
  N a / N b = N ((a * (inv (b `mod` f_p))) `mod` f_p)


data Point = O | Point
               { x :: Integer
               , y :: Integer
               } deriving Show

--pointModP :: Integer -> Integer -> Point
--pointModP x y = Point (x `mod` f_p) (y `mod` f_p)

pointModP :: Point -> Point
pointModP (Point x y) = Point (x `mod` f_p) (y `mod` f_p)

eqPoint :: Point -> Point -> Bool
eqPoint (Point x_A y_A) (Point x_B y_B) = x_A `eqMod` x_B && y_A `eqMod` y_B
eqPoint O O = True
eqPoint _ _ = False

eqMod :: Integer -> Integer -> Bool
eqMod a b = a `mod` f_p == b `mod` f_p

gcdE :: Integer -> Integer -> (Integer, Integer, Integer)
gcdE a 0 = (a, 1, 0)
gcdE a b = (d, t, s - q * t)
  where
  r         = a `mod` b
  q         = a `div` b
  (d, s, t) = gcdE b r

inv :: Integer -> Integer
inv a = t `mod` f_p
  where
  (_, _, t) = gcdE f_p a

pointDouble1 :: Point -> Point
pointDouble1 O  = O
pointDouble1 (Point x y)
  | y `eqMod` 0 = O
  | otherwise   = pointModP (Point x' y')
  where
  lambda = ((3 * x^2) * inv (2 * y `mod` f_p)) `mod` f_p
  x'     = lambda^2 - (2 * x)
  y'     = lambda * (x - x') - y

pointDoubleN :: Integer -> Point -> Point
pointDoubleN 0 p = p
pointDoubleN n p = pointDoubleN (n - 1) (pointDouble1 p)

pointAdd1 :: Point -> Point -> Point
pointAdd1 O b = pointModP b
pointAdd1 a O = pointModP a
pointAdd1 a@(Point x_A y_A) (Point x_B y_B)
  | x_A `eqMod` x_B = if y_A `eqMod` (-y_B) then O else pointDouble1 a
  | otherwise       = pointModP (Point x' y')
  where
  lambda = ((y_B - y_A) * inv ((x_B - x_A) `mod` f_p)) `mod` f_p
  x'     = lambda^2 - x_A - x_B
  y'     = lambda * (x_A - x') - y_A

prop_add :: Integer -> Integer -> Integer -> Integer -> Property
prop_add x1 y1 x2 y2 =
  y1^2 == x1^3 + 1 && y2^2 == x2^3 + 1 ==>
  (p1 `pointAdd1` p2) `eqPoint` (p2 `pointAdd1` p1)
  where
  p1 = Point x1 y1
  p2 = Point x2 y2

prop_double :: Integer -> Integer -> Bool
prop_double x y = pointDouble1 p `eqPoint` pointAdd1 p p
  where
  p = Point x y

prop_double' :: Integer -> Integer -> Property
prop_double' x y =
  y^2 == x^3 + 1 ==> (pointDouble1.pointDouble1) p `eqPoint`
  (p `pointAdd1` p `pointAdd1` p `pointAdd1` p)
  where
  p = Point x y

type DSequence = (Integer, [(Integer, Integer)])

signedWindowDecomposition :: Integer -> Integer -> DSequence
signedWindowDecomposition k r
  | k > 0 && r > 0 = decompose k 0 0
  | otherwise      = error "negative k or r"
  where
  l = getBitLen k

  decompose :: Integer -> Integer -> Integer -> DSequence
  decompose k d j
    | j <= l    = if even k then decompose (k `div` 2) d (j + 1)
                            else (d', (b_d, j) : s)
    | otherwise = (d, [])
    where
    t          = min l (j + r - 1)
    t'         = t - j
    h_d        = extractLow t' k
    (b_d, k')  = if h_d > 2^(r - 1) then (h_d - 2^r, incHigh t' k)
                                    else (h_d, k)
    (d', s)    = decompose (k' >> (t' + 1)) (d + 1) (t + 1)

-- Increment the number (k_l, k_(l-1),...,k_(t+1))
incHigh :: Integer -> Integer -> Integer
incHigh j k = (1 + (k >> (j + 1))) << (j + 1) + extractLow j k

extractLow :: Integer -> Integer -> Integer
extractLow 0 n = n `mod` 2
extractLow k n = n `mod` 2 + 2 * extractLow (k - 1) (n >> 1)

(<<) :: Integer -> Integer -> Integer
(<<) n k = shift n k (* 2)

(>>) :: Integer -> Integer -> Integer
(>>) n k = shift n k (`div` 2)

shift :: Integer -> Integer -> (Integer -> Integer) -> Integer
shift n 0 _  = n
shift n k op = shift (op n) (k - 1) op

getBitLen :: Integer -> Integer
getBitLen n = ceiling (log (fromInteger n) / log 2.0)

prop_swd :: Integer -> Integer -> Property
prop_swd k r =
  k > 0 && r > 0 ==> k == sum (map (\(b, e) -> b * 2^e) s)
  where
  (d, s) = signedWindowDecomposition k r

type Points = [(Integer, Point)]

testPointMultiply = testPm' pointMultiply
testPtM = testPm' pm

x_test :: Integer
x_test = 0x489a03c58dcf7fcfc97e99ffef0bb4634

y_test :: Integer
y_test = 0x510c6972d795ec0c2b081b81de767f808

p_test :: Point
p_test = Point x_test y_test

testPm' :: (Point -> Integer -> Point) -> String
testPm' f = showHex x " " ++ showHex y ""
  where
  (Point x y) = pointModP (f p_test 0xb8bbbc0089098f2769b32373ade8f0daf)

testWd = signedWindowDecomposition 0xb8bbbc0089098f2769b32373ade8f0daf 5

-- reference implementation
pm :: Point -> Integer -> Point
pm p k = pm' O p k

pm' :: Point -> Point -> Integer -> Point
pm' q _ 0 = q
pm' q p k
  | odd k     = pm' (pointAdd1 q p) p' k'
  | otherwise = pm' q p' k'
  where
  p' = (pointDouble1 p)
  k' = k >> 1

testPm :: Integer -> Integer -> Integer -> Property
testPm x y k =
  k > 0 ==> pm p k `eqPoint` pointMultiply p k
  where
  p = Point x y

pointMultiply :: Point -> Integer -> Point
pointMultiply a l = mainLoop (d, s) ps q
  where
  r       = 5
  (d, s)  = signedWindowDecomposition l r
  a2      = pointDouble1 a
  (ps, q) = precomputation

  precomputation :: (Points, Point)
  precomputation = (ps, q)
    where
    ps       = preLoop 1 (2^(r - 2) - 1) [(1, a), (2, a2)]
    (Just q) = lookup ((fst.last) s) ps

  preLoop :: Integer -> Integer -> Points -> Points
  preLoop i limit (p:ps)
    | i <= limit = preLoop (i + 1) limit
                  ((2 * i + 1, pointAdd1 (snd p) a2):p:ps)
    | otherwise  = p:ps

  mainLoop :: DSequence -> Points -> Point -> Point
  mainLoop (d, s) ps p = pointDoubleN ((snd.head) s) q
    where
    d' = d - 2
    q  = if 0 <= d' then loop d' (reverse s) ps p else p

  loop :: Integer -> [(Integer, Integer)] -> Points -> Point -> Point
  loop i ((b_i1, e_i1):(b_i, e_i):revSeq) ps q
    | i == 0    = q''
    | otherwise = loop (i - 1) ((b_i, e_i):revSeq) ps q''
    where
    q'  = pointDoubleN (e_i1 - e_i) q
    q'' = if b_i > 0 then pointAdd1 q' p
                     else pointAdd1 q' (pointModP (Point x (-y)))
    (Just p)           = lookup   b_i  ps
    (Just (Point x y)) = lookup ((-b_i) `mod` f_p) ps
