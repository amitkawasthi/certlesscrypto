module EllipticCurves where

import Prelude hiding ( (>>) )
import FiniteField

data Point = O | Point
               { x :: IntZp
               , y :: IntZp
               } deriving (Eq, Show)

newPoint :: Integer -> Integer -> Point
newPoint x y = Point (newIntZp x) (newIntZp y)

neg :: Point -> Point
neg (Point x y) = Point x (-y)

pointDouble1 :: Point -> Point
pointDouble1 O = O
pointDouble1 (Point x y)
  | y == 0    = O
  | otherwise = Point x' y'
  where
  lambda = (3 * x^2) / (2 * y)
  x'     = lambda^2 - (2 * x)
  y'     = lambda * (x - x') - y

pointDoubleN :: Integer -> Point -> Point
pointDoubleN 0 p = p
pointDoubleN n p = pointDoubleN (n - 1) (pointDouble1 p)

pointAdd1 :: Point -> Point -> Point
pointAdd1 O b = b
pointAdd1 a O = a
pointAdd1 a@(Point x_A y_A) (Point x_B y_B)
  | x_A == x_B = if y_A == (-y_B) then O else pointDouble1 a
  | otherwise  = Point x' y'
  where
  lambda = (y_B - y_A) / (x_B - x_A)
  x'     = lambda^2 - x_A - x_B
  y'     = lambda * (x_A - x') - y_A

type DSequence = (Integer, [(Integer, Integer)])

signedWindowDecomposition :: Integer -> Integer -> DSequence
signedWindowDecomposition k r
  | k > 0 && r > 0 = decompose k 0 0
  | otherwise      = error "negative k or r"
  where
  l = getBitLen k

  decompose :: Integer -> Integer -> Integer -> DSequence
  decompose k d j
    | j <= l    = if even k then decompose (k `div` 2) d (j + 1)
                            else (d', (b_d, j) : s)
    | otherwise = (d, [])
    where
    t          = min l (j + r - 1)
    t'         = t - j
    h_d        = extractLow t' k
    (b_d, k')  = if h_d > 2^(r - 1) then (h_d - 2^r, incHigh t' k)
                                    else (h_d, k)
    (d', s)    = decompose (k' >> (t' + 1)) (d + 1) (t + 1)

-- Increment the number (k_l, k_(l-1),...,k_(t+1))
incHigh :: Integer -> Integer -> Integer
incHigh j k = (1 + (k >> (j + 1))) << (j + 1) + extractLow j k

extractLow :: Integer -> Integer -> Integer
extractLow 0 n = n `mod` 2
extractLow k n = n `mod` 2 + 2 * extractLow (k - 1) (n >> 1)

getBitLen :: Integer -> Integer
getBitLen n = ceiling (log (fromInteger n) / log 2.0)

type Points = [(Integer, Point)]

pointMultiply :: Point -> Integer -> Point
pointMultiply a l = mainLoop (d, s) ps q
  where
  r       = 5
  (d, s)  = signedWindowDecomposition l r
  a2      = pointDouble1 a
  (ps, q) = precomputation

  precomputation :: (Points, Point)
  precomputation = (ps, q)
    where
    ps       = preLoop 1 (2^(r - 2) - 1) [(1, a), (2, a2)]
    (Just q) = lookup ((fst.last) s) ps

  preLoop :: Integer -> Integer -> Points -> Points
  preLoop i limit (p:ps)
    | i <= limit = preLoop (i + 1) limit
                  ((2 * i + 1, pointAdd1 (snd p) a2):p:ps)
    | otherwise  = p:ps

  mainLoop :: DSequence -> Points -> Point -> Point
  mainLoop (d, s) ps p = pointDoubleN ((snd.head) s) q
    where
    d' = d - 2
    q  = if 0 <= d' then loop d' (reverse s) ps p else p

  loop :: Integer -> [(Integer, Integer)] -> Points -> Point -> Point
  loop i ((b_i1, e_i1):(b_i, e_i):revSeq) ps q
    | i == 0    = q''
    | otherwise = loop (i - 1) ((b_i, e_i):revSeq) ps q''
    where
    q'  = pointDoubleN (e_i1 - e_i) q
    q'' = if b_i > 0 then pointAdd1 q' p
                     else pointAdd1 q' (neg p')
    (Just p)  = lookup b_i  ps
    (Just p') = lookup (modFp (-b_i)) ps

type JPoint = (IntZp, IntZp, IntZp)

--newJPoint :: Integer -> Integer -> Point
--newJPoint x y = Point (newIntZp x) (newIntZp y)

projectivePointDouble1 :: JPoint -> JPoint
projectivePointDouble1 (x, y, z)
  | z == 0 || y == 0 = (0, 1, 0)
  | otherwise        = (x', y', z')
  where
  lambda_1 = 3 * x^2
  z'       = 2 * y * z
  lambda_2 = y^2
  lambda_3 = 4 * lambda_2 * x
  x'       = lambda_1^2 - 2 * lambda_3
  lambda_4 = 8 * lambda_2^2
  y'       = lambda_1 * (lambda_3 - x') - lambda_4

projectivePointAccumulate1 :: JPoint -> Point -> JPoint
projectivePointAccumulate1 _ O = error "O point not allowed"
projectivePointAccumulate1 (x_A, y_A, z_A) (Point x_B y_B)
  | z_A == 0      = (x_B, y_B, 1)
  | lambda_3 == 0 = (0, 1, 0)
  | otherwise     = (x', y', z')
  where
  lambda_1 = z_A^2
  lambda_2 = lambda_1 * x_B
  lambda_3 = x_A - lambda_2
  lambda_4 = lambda_3^2
  lambda_5 = lambda_1 * y_B * z_A
  lambda_6 = lambda_4 - lambda_5
  lambda_7 = x_A + lambda_2
  lambda_8 = y_A + lambda_5
  x'       = lambda_6^2 - lambda_7 * lambda_4
  lambda_9 = lambda_7 * lambda_4 - 2 * x'
  y'       = (lambda_9 * lambda_6 - lambda_8 * lambda_3 * lambda_4) / 2
  z'       = lambda_3 * z_A

evalVertical1 :: Point -> Point -> IntZp
evalVertical1 O _ = error "O point not allowed"
evalVertical1 (Point x_B _) (Point x_A _) = x_B - x_A

     -- f_p != f_p^2 !!!!!!!!!!!!!!!!!!!!!!!!

evalTangent1 :: Point -> Point -> IntZp
evalTangent1 O _ = error "O point not allowed"
evalTangent1 _ O = 1
evalTangent1 b a@(Point _ 0)                 = evalVertical1 b a
evalTangent1 (Point x_B y_B) (Point x_A y_A) = a * x_B + b * y_B + c
  where
  a = -3 * (x_A)^2
  b =  2 * y_A
  c = -b * y_A - a * x_A

evalLine1 :: Point -> Point -> Point -> IntZp
evalLine1 b O  a'' = evalVertical1 b a''
evalLine1 b a' O   = evalVertical1 b a'
evalLine1 b@(Point x_B y_B) a'@(Point x_A' y_A') a''@(Point x_A'' y_A'')
  | a' == neg a'' = evalVertical1 b a'
  | a' == a''     = evalTangent1  b a'
  | otherwise     = a * x_B + b' * y_B + c
  where
  a  = y_A' - y_A''
  b' = x_A'' - x_A'
  c  = -b' * y_A' - a * x_A'

f_q = newIntZp 0xfffffffffffffffffffffffffffbffff

tate :: Point -> Point -> IntZp
tate pA pB = tateMillerSolinas pA pB ssec

pairing1 :: Point -> Point -> (IntZp, IntZp)
pairing1 pA (Point x y) = (tateMillerSolinas pA (Point xa y) ssec,
                           tateMillerSolinas pA (Point xb y) ssec)
  where
  a_zeta        = newIntZp (f_p - 1) / (N 2)
  (N b_zetaExp) = newIntZp (f_p + 1) / (N 4)
  b_zeta        = (N 3) `pow` b_zetaExp
  xa            = x * a_zeta
  xb            = x * b_zeta

type Curve  = (IntZp, IntZp, IntZp, IntZp, IntZp)
type Params = (IntZp, IntZp, JPoint, IntZp, IntZp)

ssec :: Curve
ssec = (f_q, newIntZp 128, newIntZp (-1), newIntZp 18, newIntZp (-1))

tateMillerSolinas :: Point -> Point -> Curve -> IntZp
tateMillerSolinas pA@(Point x_A y_A) pB@(Point x_B y_B)
  curve@(q, _, _, _, _)   = result
  where
  one                     = newIntZp 1
  params                  = (one, one, (x_A, y_A, one), one, one)
  (v_B, params')          = computeS2b curve pB params
  (v_A, params'')         = compute2a  curve pB params'
  (v_num, v_den, _, _, _) = correct pA pB v_A v_B curve params''
  (N eta)                 = newIntZp (f_p^2 - 1) / q
  result                  = (v_num / v_den) `pow` eta

computeS2b :: Curve -> Point -> Params -> (Point, Params)
computeS2b (_, _, s, b, _) pB params = (v_b, accS2b s pB p)
  where
  p               = repDouble (newIntZp 0) b pB params
  (_, _, v, _, _) = p
  (x_V, y_V, z_V) = v
  v_b             = Point (x_V / z_V^2) (s * y_V / z_V^3)

accS2b :: IntZp -> Point -> Params -> Params
accS2b s pB (v_num, v_den, (x_V, y_V, z_V), t_num, t_den)
  | s == (newIntZp (-1)) = (v_num * t_den, v_den * t_num *
                           evalVertical1 pB (Point
                           (x_V / z_V^2) (y_V / z_V^3)), (x_V, y_V, z_V),
                           t_num, t_den)
  | s ==  (newIntZp 1)   = (v_num * t_num, v_den * t_den,
                           (x_V, y_V, z_V), t_num, t_den)
  | otherwise            = error "incorrect s value"

-- s, a... in Integer or IntZp ???

repDouble :: IntZp -> IntZp -> Point -> Params -> Params
repDouble (N n) (N lim) pB params@(v_num, v_den, v, t_num, t_den)
  | n < lim   = repDouble (N (n + 1)) (N lim) pB params'
  | otherwise = params
  where
  params'            = (v_num, v_den, v2, t_num', t_den')
  (x_V , y_V , z_V)  = v
  t_num'             = t_num^2 * evalTangent1 pB
                       (Point (x_V / z_V^2) (y_V / z_V^3))
  v2                 = projectivePointDouble1 v
  (x_V', y_V', z_V') = v2
  t_den'             = t_den^2 * evalVertical1 pB
                       (Point (x_V' / z_V'^2) (y_V' / z_V'^3))

compute2a :: Curve -> Point -> Params -> (Point, Params)
compute2a (_, a, s, b, _) pB params = (v_a, acc2a p)
  where
  p               = repDouble b a pB params
  (_, _, v, _, _) = p
  (x_V, y_V, z_V) = v
  v_a             = Point (x_V / z_V^2) (s * x_V / z_V^3)

acc2a :: Params -> Params
acc2a (v_num, v_den, v, t_num, t_den) =
  (v_num * t_num, v_den * t_den, v, t_num, t_den)

correct :: Point -> Point -> Point -> Point -> Curve -> Params -> Params
correct pA pB v_A v_B (_, _, _, _, c) (v_num, v_den, v, t_num, t_den) =
  (v_num', v_den' * c', v, t_num, t_den)
  where
  v_num' = v_num * evalLine1 pB v_A v_B
  v_den' = v_den * evalVertical1 pB (v_A `pointAdd1` v_B)
  c'     = if c == (newIntZp (-1)) then evalVertical1 pB pA else (newIntZp 1)